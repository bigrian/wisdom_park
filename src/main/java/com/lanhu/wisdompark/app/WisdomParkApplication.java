package com.lanhu.wisdompark.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.lanhu.wisdompark.app.mapper")
public class WisdomParkApplication {

    public static void main(String[] args) {
        SpringApplication.run(WisdomParkApplication.class, args);
        System.out.println(
                        "                            _ooOoo_  \n" +
                        "                           o8888888o  \n" +
                        "                          88\" . \"88  \n" +
                        "                           (| -_- |)  \n" +
                        "                            O\\ = /O  \n" +
                        "                        ____/`---'\\____  \n" +
                        "                      .   ' \\\\| |// `.  \n" +
                        "                       / \\\\||| : |||// \\  \n" +
                        "                     / _||||| -:- |||||- \\  \n" +
                        "                       | | \\\\\\ - /// | |  \n" +
                        "                     | \\_| ''\\---/'' | |  \n" +
                        "                      \\ .-\\__ `-` ___/-. /  \n" +
                        "                   ___`. .' /--.--\\ `. . __  \n" +
                        "                .\"\" '< `.___\\_<|>_/___.' >'\"\".  \n" +
                        "               | | : `- \\`.;`\\ _ /`;.`/ - ` : | |  \n" +
                        "                 \\ \\ `-. \\_ __\\ /__ _/ .-` / /  \n" +
                        "          ======`-.____`-.___\\_____/___.-`____.-'======  \n" +
                        "                            `=---='  \n" +
                        "  \n" +
                        "         .............................................  \n" +
                        "                  佛祖保佑             永无BUG \n" +
                        "          佛曰:  \n" +
                        "                  写字楼里写字间，写字间里程序员；  \n" +
                        "                  程序人员写程序，又拿程序换酒钱。  \n" +
                        "                  酒醒只在屏前坐，酒醉还来屏下眠；  \n" +
                        "                  酒醉酒醒日复日，屏上屏下年复年。  \n" +
                        "                  诗酒代码常相伴，不愿鞠躬车马前；  \n" +
                        "                  奔驰宝马富者趣，调试代码程序员。  \n" +
                        "                  别人笑我忒疯癫，我笑他人看不穿；  \n" +
                        "                  虽无豪宅亦无钱，饮酒笑傲代码间？  \n");
    }

}
