package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author whongyu
 * @create by 2019/5/25
 */
@Setter
@Getter
public class UserGarden {

    private int id;

    private int userId;

    private String gardenId;
}
