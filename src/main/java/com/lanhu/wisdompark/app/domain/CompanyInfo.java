package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 入驻企业信息表
 * @author whongyu
 * @create by 2019/5/20
 */
@Getter
@Setter
public class CompanyInfo {

    //主键(uuid)
    private String id;
    //企业名称
    private String companyName;
    //企业简介
    private String describe;
    //园区id
    private String garderId;
    //位置
    private String place;
    //企业logo
    private String logo;
    //企业类型
    private Integer type;
    //规模/人
    private Integer scale;
    //企业照片/执照
    private String license;
    //所属业态
    private Integer formats;
    //是否开放加盟 0开放 1否
    private Integer isLeague;
    //企业联系人姓名
    private String linkManName;
    //联系人手机号
    private String mobile;
    //备用字段 json结构
    private String spareField;
    //状态 0已入驻  1已迁出
    private Integer status;
    //创建时间
    private String createTime;
    //更新时间
    private String updateTime;

    //扩展字段
    private int countCompany;
}
