package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 字典表
 * @author whongyu
 * @create by 2019/5/20
 */
@Getter
@Setter
public class Dict {

    //主键
    private Integer id;
    //名称
    private String name;
    //code值
    private Integer code;
    //类型
    private String type;
    //父节点
    private Integer parentId;
    //排序
    private Integer sort;
    //备注
    private String remark;
    //状态 0启用 1禁用 (非开发人员不可更改)
    private Integer status;
    //创建时间
    private String createTime;
}
