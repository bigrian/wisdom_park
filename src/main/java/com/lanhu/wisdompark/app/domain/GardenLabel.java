package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 园区标签表
 * @author whongyu
 * @create by 2019/5/27
 */
@Setter
@Getter
public class GardenLabel {

    //主键
    private Integer id;
    //标签名称
    private String labelName;
    //园区id
    private String gardenId;
    //图标地址
    private String icon;
    //排序
    private Integer sort;
    //是否是可跳转  0 否 1是
    private Integer isSkip;
    //状态：0显示  1隐藏
    private Integer status;
    //创建时间
    private String createTime;

}
