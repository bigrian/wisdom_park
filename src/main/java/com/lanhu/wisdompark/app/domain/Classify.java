package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/20
 */
@Getter
@Setter
public class Classify {

    //主键
    private Integer id;
    //分类名称
    private String name;
    //分类级别
    private Integer level;
    //父节点id
    private Integer parentId;
    //分类类别
    private Integer type;
    //排序
    private Integer sort;
    //备注
    private String remark;
    //状态 0启用  1禁用  2删除
    private Integer status;
    //创建时间
    private String createTime;

    //扩展字段
    List<Classify> classifies;
}
