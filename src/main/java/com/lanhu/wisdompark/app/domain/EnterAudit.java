package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 入驻企业审核表
 * @author whongyu
 * @create by 2019/5/20
 */
@Getter
@Setter
public class EnterAudit {

    ///主键
    private Integer id;
    //企业名称
    private String companyName;
    //所属业态
    private Integer formats;
    //联系人
    private String linkManName;
    //联系人手机号
    private String linkMobile;
    //产业园id(意向业态)
    private String gardenId;
    //户型id(意向户型)
    private Integer unitId;
    //意向面积
    private String intentionArea;
    //备用字段  json结构
    private String spare;
    //移动端用户Id(企业用户角色)/申请人
    private Integer wxAppUserId;
    //审核状态 1待审核  2已通过
    private Integer status;
    //申请时间/创建时间
    private String createTime;

}
