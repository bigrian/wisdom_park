package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 系统菜单表
 * @author whongyu
 * @create by 2019/5/20
 */
@Getter
@Setter
public class Menu {

    //
    private Integer id;
    //菜单名称
    private String menuName;
    //菜单图标
    private String icon;
    //父节点（一级菜单为1 默认 1）
    private Integer parentId;
    //
    private String action;
    //
    private String module;
    //用户类型
    private String userType;
    //创建时间
    private String createTime;
    //状态 0正常  1 删除
    private Integer status;

    //扩展字段
    private List<Menu> menuList;
}
