package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/20
 */
@Getter
@Setter
public class BusinessMovement {

    //主键(uuid)
    private String id;
    //标题
    private String title;
    //内容
    private String content;
    //首图 path
    private String headImgPath;
    //公司id(uuid)
    private String companyId;
    //类型
    private Integer type;
    //状态 0  已发布  1已删除
    private Integer status;
    //发布时间
    private String createTime;
    //修改时间
    private String updateTime;

    //扩展字段
    private List<WpResource> wpResourceList;
}
