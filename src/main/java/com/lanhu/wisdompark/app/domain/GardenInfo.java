package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * 园区信息表
 * @author whongyu
 * @create by 2019/5/20
 */
@Setter
@Getter
public class GardenInfo {

    //主键(uuid)
    private String id;
    //园区名称
    private String name;
    //园区简称
    private String shortName;
    //省
    private String province;
    //市
    private String city;
    //区/县
    private String county;
    //地址
    private String address;
    //楼层
    private Integer floorPlan;
    //项目体量
    private Double area;
    //园区描述
    private String describe;
    //开业时间
    private String openDate;
    //访问人次
    private Integer counts;
    //首页展示图 路径
    private String headImgPath;
    //pdf 文件路径
    private String pdfPath;
    //经度
    private Double longitude;
    //纬度
    private Double latitude;
    //排序
    private int sort;
    //状态 是否启用 0启用 1禁用
    private int status;
    //是否设为推荐 0启用 1禁用
    private int isRecommend;
    //微信小程序appid
    private String appId;
    //微信小程序appSecret
    private String secret;
    //创建时间
    private String createTime;
    //更新时间
    private String updateTime;

    //扩展字段

    private List<WxAppUser> wxAppUserList;

    private List<CompanyInfo> companyInfoList;

    private List<WpResource> wpResourceList;

    private WpResource wpResourcePdf;

    private PolicyNews policyNews;

    private int userId;
}
