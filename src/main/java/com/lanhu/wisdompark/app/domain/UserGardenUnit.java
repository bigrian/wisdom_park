package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author whongyu
 * @create by 2019/5/25
 */
@Setter
@Getter
public class UserGardenUnit {

    private int id;
    //微信小程序用户id
    private int wxAppUserId;
    //园区户型id
    private int gardenUnitId;
    //园区id
    private String gardenId;
    //类型
    private int type;

    //扩展字段
    private int countAppoint;

    private String headPath;
}
