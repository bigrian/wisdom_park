package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/25
 */
@Setter
@Getter
public class GardenUnit {

    //主键
    private int id;
    //园区id
    private String gardenId;
    //园区户型图编号
    private String gardenUnitNum;
    //描述
    private String describe;
    //路径
    private String path;
    //  所属楼层
    private String floorPlan;
    //面积
    private double area;
    //bitmap_path楼层落位图
    private String bitmapPath;
    //排序
    private int sort;
    //是否启用   0启用  1禁用
    private int status;

    private String createTime;
    //扩展字段
    private int counts;

    List<WxAppUser> wxAppUserList;

    List<UserGardenUnit> headPaths;

    private int pageNum = 1;

    private int pageSize = 10;

}
