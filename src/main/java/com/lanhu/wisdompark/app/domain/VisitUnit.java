package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 *  访问者与商铺关系表
 * @author whongyu
 * @create by 2019/5/20
 */
@Setter
@Getter
public class VisitUnit {

    //主键
    private Integer id;
    //户型id
    private int unitId;
    //小程序 app用户id
    private Integer wxAppUserId;
}
