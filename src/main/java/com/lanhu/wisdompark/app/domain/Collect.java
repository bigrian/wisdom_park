package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户收藏表
 * @author whongyu
 * @create by 2019/5/20
 */
@Getter
@Setter
public class Collect {

    //主键
    private Integer id;
    //用户id
    private Integer userId;
    //园区id/商铺id
    private String gardenShopId;
    //收藏时间
    private String createTime;
}
