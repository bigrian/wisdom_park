package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 微信小程序接口返回结果实体类
 * @author whongyu
 * @create by 2019/5/22
 */
@Setter
@Getter
@ToString
public class WeChatRes {

    private String openid;  // 用户唯一标识

    private String session_key;// 会话密钥

    private String unionid;// 用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回

    /**
     * 错误码：
     -1	系统繁忙，此时请开发者稍候再试
     0	请求成功
     40029	code 无效
     45011	频率限制，每个用户每分钟100次
     */
    private int errcode;

    private String errMsg;// 错误信息

    private String access_token;//接口凭证 access_token  正常返回 {"access_token":"ACCESS_TOKEN","expires_in":7200}

    private int expires_in;
}
