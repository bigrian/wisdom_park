package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 *  访问者与园区关系表
 * @author whongyu
 * @create by 2019/5/20
 */
@Getter
@Setter
public class VisitGarden {

    //主键
    private Integer id;
    //微信app用户id
    private Integer wxAppUserId;
    //园区id
    private String gardenId;

    //扩展字段
    private int counts;
}
