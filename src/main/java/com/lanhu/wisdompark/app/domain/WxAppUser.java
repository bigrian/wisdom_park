package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 *  app微信小程序用户表
 * @author whongyu
 * @create by 2019/5/20
 */
@Setter
@Getter
public class WxAppUser {

    //主键
    private Integer id;
    //手机号
    private String mobile;
    //账号
    private String userName;
    //密码
    private String passwork;
    //昵称
    private String nickName;
    //头像路径
    private String headPath;
    //省
    private String province;
    //市
    private String city;
    //区
    private String county;
    //地址
    private String address;
    //微信小程序openId
    private String openId;
    //微信小程序session_key
    private String sessionKey;
    //微信小程序内unionid
    private String unionid;
    //微信返回的用户所在国家
    private String country;
    //性别 0 未知 1男 2女
    private Integer sex;
    //0 启用 2 禁用
    private Integer status;
    //类型 0访问者 1企业用户
    private Integer type;
    //创建时间
    private String createTime;
    //修改时间
    private String updateTime;

    //扩展字段
    private Date insertTime;

    private double longitude;

    private double latitude;
}
