package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 资源表(视频/图片等)
 * @author whongyu
 * @create by 2019/5/20
 */
@Setter
@Getter
public class WpResource {

    //主键
    private Integer id;
    //园区id或企业id或活动新闻表id或企业动态表id(uuid)
    private String resourceId;
    //资源路径
    private String path;
    //类型  0图片 1普通视频 2VR视频 3pdf文件 4楼层落位图
    private Integer type;
    //排序
    private int sort;
    //创建时间
    private String createTime;
}
