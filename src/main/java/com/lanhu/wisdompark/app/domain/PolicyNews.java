package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 政策新闻/项目活动表
 * @author whongyu
 * @create by 2019/5/20
 */
@Setter
@Getter
public class PolicyNews {

    //主键(uuid)
    private String id;
    //标题
    private String title;
    //发布内容
    private String content;
    //所在位置
    private String place;
    //类型 1政府动态 2媒体资源 3 社会新闻 0不划分分类
    private Integer type;
    //园区id
    private String gardenId;
    //状态 0 正常 1过期 2删除

    private String headImgPath;

    private Integer status;
    //发布时间
    private String createTime;
    //修改时间
    private String updateTime;
}
