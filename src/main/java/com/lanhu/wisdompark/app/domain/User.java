package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 *  系统用户表
 * @author whongyu
 * @create by 2019/5/20
 */
@Setter
@Getter
public class User {

    //
    private Integer id;
    //账号
    private String userName;
    //密码
    private String password;
    //手机号
    private String mobile;
    //省
    private String province;
    //市
    private String city;
    //县/区
    private String county;
    //地址
    private String address;
    //用户类型  0超级管理员（蓝湖） 1园区管理员
    private Integer type;
    //状态 0启用 1禁用
    private int status;
    //创建时间
    private String createTime;
    //更新时间
    private String updateTime;
}
