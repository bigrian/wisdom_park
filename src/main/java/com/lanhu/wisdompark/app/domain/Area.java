package com.lanhu.wisdompark.app.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 城市区域
 * @author lfxa
 *
 */
@Getter
@Setter
public class Area {

	private int id;
	
	private String name;
	
	private String code;
	
	private String parentCode;
	
	private String centralPoint;
	
	private String rings;
	
	private int areaLevel;
	
	private String provinceCode = "";
	
	private String cityCode = "";
	
	private String countyCode = "";
	
	private int townId;

}
