package com.lanhu.wisdompark.app.constant;

/**
 * 公共常量类
 * @Description:
 */
public class Constants {

    public static final String REQ = "req|";//接口日志标识：req表示一开始进入接口时候日志
    public static final String RES = "res|";//接口日志标识：res表示接口处理完结束时候的日志


}
