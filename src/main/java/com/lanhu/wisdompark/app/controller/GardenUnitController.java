package com.lanhu.wisdompark.app.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.GardenUnit;
import com.lanhu.wisdompark.app.service.GardenUnitService;
import com.lanhu.wisdompark.app.util.Result;
import com.lanhu.wisdompark.app.util.UploadUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/25
 */
@RestController
@Slf4j
@RequestMapping("/api/garden/unit")
@Api(tags={"PC-园区下-园区户型相关接口服务"})
@CrossOrigin
public class GardenUnitController {

    @Autowired
    private GardenUnitService gardenUnitService;
    @Autowired
    private UploadUtils uploadUtils;

    @ApiOperation(value = "上传户型图接口")
    @RequestMapping(value = "/uploadUnit", method = RequestMethod.POST)
    @ResponseBody
    public Result uploadUnit(@RequestParam MultipartFile file,@RequestParam String gardenId){
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/unit/uploadFile|上传户型图接口,file: "+ file);
        try{
            List<GardenUnit> gardenUnits = new ArrayList<>();
            int count = 0;
                String paths = uploadUtils.uploadBlob(file, "parkfile");//parkfile 为blob容器名称
                if (!"".equals(paths)){
                    GardenUnit gardenUnit = new GardenUnit();
                    gardenUnit.setPath(paths);
                    gardenUnit.setGardenId(gardenId);
                    gardenUnit.setSort(count++);
                    gardenUnitService.addGardenUnit(gardenUnit);
                    gardenUnits.add(gardenUnit);

                }else{
                    return new Result(ErrorCode.E_10001);
                }

            log.info(Constants.RES + ErrorCode.SUCCESS.getCode() + "|api/garden/unit/uploadFile|上传户型图接口,gardenUnits: " + gardenUnits);
            return new Result(ErrorCode.SUCCESS,gardenUnits);
        }catch (Exception e){
            log.error(Constants.RES + ErrorCode.E_10001.getCode()+ "|api/garden/unit/uploadFile|上传户型图接口", e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ApiOperation(value = "上传楼层落位图接口")
    @RequestMapping(value = "/uploadBitmap", method = RequestMethod.POST)
    @ResponseBody
    public Result uploadBitmap(@RequestParam MultipartFile file,@RequestParam int unitId){
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/unit/uploadBitmap|上传楼层落位图接口,file: "+ file);
        try{
            if(0 == unitId){
                return new Result(ErrorCode.E_20015);
            }
            List<GardenUnit> gardenUnitList = new ArrayList<>();
            String paths = uploadUtils.uploadBlob(file, "parkfile");//parkfile 为blob容器名称
            if (!"".equals(paths)){
                GardenUnit gardenUnit = gardenUnitService.findGardenUnitById(unitId);
                gardenUnit.setBitmapPath(paths);
                System.out.println("gardenUnit:"+ JSON.toJSON(gardenUnit));
                gardenUnitService.updateGardenUnit(gardenUnit);
                gardenUnitList.add(gardenUnit);

            }else{
                return new Result(ErrorCode.E_10001);
            }
            log.info(Constants.RES + ErrorCode.SUCCESS.getCode() + "|api/garden/unit/uploadBitmap|上传楼层落位图接口,gardenUnitList: " + gardenUnitList);
            return new Result(ErrorCode.SUCCESS,gardenUnitList);
        }catch (Exception e){
            log.error(Constants.RES + ErrorCode.E_10001.getCode()+ "|api/garden/unit/uploadBitmap|上传楼层落位图接口", e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PutMapping(value = "/add")
    @ApiOperation(value = "添加户型相关信息")
    public Result findGardenById(@RequestBody GardenUnit gardenUnit) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/unit/add|根据id查询园区信息:gardenUnit={}",gardenUnit);
        try {
            int i = gardenUnitService.addGardenById(gardenUnit);
            if(-1 == i){
                return new Result(ErrorCode.E_20021);
            }else {
                return new Result(ErrorCode.SUCCESS);
            }
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "某园区下户型相关信息接口")
    public Result findGarden(@RequestParam String gardenId,@RequestParam(defaultValue = "1") int pageNum,@RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/unit/find|某园区下户型相关信息接口:gardenId={}",gardenId);
        try {
            PageInfo<GardenUnit> pageInfo = gardenUnitService.findGardenByGardenId(gardenId,pageNum,pageSize);
            return new Result(ErrorCode.SUCCESS,pageInfo);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "根据id查询户型相关信息接口")
    public Result findGardenById(@RequestParam int id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/unit/query|根据id查询户型相关信息接口:id={}",id);
        try {
            GardenUnit gardenUnit = gardenUnitService.findGardenById(id);
            return new Result(ErrorCode.SUCCESS,gardenUnit);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/search")
    @ApiOperation(value = "多条件查询户型相关信息接口（gardenId 必传）")
    public Result searchGarden(@RequestBody GardenUnit gardenUnit) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/unit/search|多条件查询户型相关信息接口（gardenId 必传）:gardenUnit={}",gardenUnit);
        try {
            PageInfo<GardenUnit> gardenUnits = gardenUnitService.searchGarden(gardenUnit);
            return new Result(ErrorCode.SUCCESS,gardenUnits);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @DeleteMapping (value = "/remove")
    @ApiOperation(value = "根据id删除户型信息接口")
    public Result removeGardenById(@RequestParam int id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/unit/remove|根据id删除户型信息接口:id={}",id);
        try {
            int i = gardenUnitService.removeGardenById(id);
            if(i>0){
                return new Result(ErrorCode.SUCCESS);
            }
            return new Result(ErrorCode.E_20025);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
