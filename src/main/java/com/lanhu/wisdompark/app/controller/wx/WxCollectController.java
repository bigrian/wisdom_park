package com.lanhu.wisdompark.app.controller.wx;

import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.Collect;
import com.lanhu.wisdompark.app.service.wx.WxCollectService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/22
 */
@RestController
@Slf4j
@RequestMapping("/api/wx/collect")
@Api(tags={"微信小程序-用户收藏相关接口服务"})
@CrossOrigin
public class WxCollectController {

    @Autowired
    private WxCollectService wxCollectService;

    @ResponseBody
    @PostMapping(value = "/operate")
    @ApiOperation(value = "用户收藏园区/商铺接口(type 0 收藏 1取消收藏)")
    public Result userCollectOperate(@RequestParam String collectId,@RequestParam int userId,@RequestParam int type) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/collect/operate|用户收藏园区/商铺接口:garendId={},UserId={}", collectId,userId);
        try {
            wxCollectService.userCollectOperate(collectId,userId,type);
            return new Result(ErrorCode.SUCCESS);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "查询用户收藏列表")
    public Result findUserCollect(@RequestParam int userId) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/collect/find|查询用户收藏列表:userId={}",userId);
        try {
            List<Collect> collects = wxCollectService.findUserCollect(userId);
            return new Result(ErrorCode.SUCCESS,collects);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
