package com.lanhu.wisdompark.app.controller;

import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.WpResource;
import com.lanhu.wisdompark.app.service.WpResourceService;
import com.lanhu.wisdompark.app.util.Result;
import com.lanhu.wisdompark.app.util.UploadUtils;
import com.lanhu.wisdompark.app.util.UuidUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/24
 */
@RestController
@Slf4j
@RequestMapping("/api/advert")
@Api(tags={"PC-首页广告位相关接口服务(微信小程序)"})
@CrossOrigin
public class AdvertisementController {

    @Autowired
    private WpResourceService wpResourceService;

    @ApiOperation(value = "首页广告位接口 支持批量上传")
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ResponseBody
    public Result upload(MultipartFile[] files,@RequestParam String gardenId){
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/common/img/uploadFile|上传图片接口,files={},gardenId={}: "+ files,gardenId);
        try{
            UploadUtils upload = null;
            WpResource wpResource = null;
            List<String> pathList = new ArrayList<>();
            int count = 0;
            for (MultipartFile file :files){
                upload = new UploadUtils();
                String paths = upload.uploadBlob(file, "parkfile");//parkfile 为blob容器名称
                if (!"".equals(paths)){
                    String uuid = UuidUtil.getUUID();
                    wpResource = new WpResource();
                    wpResource.setType(3);
                    wpResource.setResourceId(gardenId);
                    wpResource.setResourceId(uuid);
                    wpResource.setPath(paths);
                    wpResource.setSort(count++);
                    wpResourceService.addWpResource(wpResource);
                    pathList.add(paths);

                }else{
                    return new Result(ErrorCode.E_10001);
                }
            }

            log.info(Constants.RES + ErrorCode.SUCCESS.getCode() + "|api/common/img/uploadFile|上传图片接口,pathList: " + pathList);
            return new Result(ErrorCode.SUCCESS,pathList);
        }catch (Exception e){
            log.error(Constants.RES + ErrorCode.E_10001.getCode()+ "|api/common/img/uploadFile|上传图片接口报错", e);
            return new Result(ErrorCode.E_10001);
        }
    }

}
