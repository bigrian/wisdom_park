package com.lanhu.wisdompark.app.controller.wx;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.GardenInfo;
import com.lanhu.wisdompark.app.service.wx.WxGardenInfoService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * @author whongyu
 * @create by 2019/5/21
 */
@RestController
@Slf4j
@RequestMapping("/api/wx/garden")
@Api(tags={"微信小程序-园区相关接口服务"})
@CrossOrigin
public class WxGardenInfoController {

    @Autowired
    private WxGardenInfoService wxGardenInfoService;

    @ResponseBody
    @GetMapping(value = "/list")
    @ApiOperation(value = "查询该集团用户下所有园区")
    public Result findAll(@RequestParam String gardenId,
                          @RequestParam(defaultValue = "1") int pageNum,
                          @RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/garden/list|查询所有园区:gardenId={},pageNum={},pageSize={}",gardenId, pageNum, pageSize);
        try {
            PageInfo<GardenInfo> gardenInfos = wxGardenInfoService.findAllUserDown(gardenId,pageNum,pageSize);
            return new Result(ErrorCode.SUCCESS,gardenInfos);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/findDetail")
    @ApiOperation(value = "根据id查询园区详情")
    public Result findGardenById(@RequestParam String gardenId,@RequestParam int userId) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/garden/findDetail|查询所有园区:garendId={},garendId={}", gardenId,userId);
        try {
            GardenInfo gardenInfos = wxGardenInfoService.findGardenById(gardenId,userId);
            return new Result(ErrorCode.SUCCESS,gardenInfos);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
