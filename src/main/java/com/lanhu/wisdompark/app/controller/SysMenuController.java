package com.lanhu.wisdompark.app.controller;

import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.Menu;
import com.lanhu.wisdompark.app.service.SysMenuService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/6/26
 */
@RestController
@Slf4j
@RequestMapping("/api/sys/menu")
@Api(tags={"PC-菜单管理相关接口服务"})
@CrossOrigin
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;

    @ResponseBody
    @PutMapping(value = "/add")
    @ApiOperation(value = "创建N级菜单接口")
    public Result findGardenById(@RequestBody Menu menu) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/sys/menu/add|创建N级菜单接口={}",menu);
        try {
            int i = sysMenuService.createMenu(menu);
            if(i>0){
                return new Result(ErrorCode.SUCCESS);
            }else {
                return new Result(ErrorCode.E_20029);
            }
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "根据用户类型查询N级菜单接口")
    public Result findMenu(@RequestParam String userType) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/sys/menu/add|根据用户类型查询N级菜单接口 userType={}",userType);
        try {
            List<Menu> menus = sysMenuService.findMenuByUserType(userType);
            return new Result(ErrorCode.SUCCESS,menus);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PostMapping(value = "/edit")
    @ApiOperation(value = "编辑菜单信息接口")
    public Result editMenu(@RequestBody Menu menu) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/sys/menu/edit|编辑菜单信息接口 menu={}",menu);
        try {
            int i = sysMenuService.editMenu(menu);
            if(i>0){
                return new Result(ErrorCode.SUCCESS);
            }else {
                return new Result(ErrorCode.E_20029);
            }
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "根据id菜单信息接口")
    public Result queryMenu(@RequestParam int id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/sys/menu/query|根据id菜单信息接口 id={}",id);
        try {
            Menu menu = sysMenuService.queryMenu(id);
            return new Result(ErrorCode.SUCCESS,menu);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
