package com.lanhu.wisdompark.app.controller.wx;

import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.WpResource;
import com.lanhu.wisdompark.app.service.WpResourceService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/6/10
 */
@RestController
@Slf4j
@RequestMapping("/api/wx/advertise")
@Api(tags={"微信小程序-广告/首页相关接口"})
@CrossOrigin
public class WxAdvertiseController {

    @Autowired
    private WpResourceService wpResourceService;

    @ApiOperation(value = "首页轮播图展示")
    @GetMapping(value = "/list")
    @ResponseBody
    public Result findAdvert(@RequestParam String gardenId){
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|/api/wx/advertise/list|首页轮播图展示:gardenId = {}"+gardenId);
        try{
            List<WpResource> wpResources = wpResourceService.findAdvert(gardenId);
            return new Result(ErrorCode.SUCCESS,wpResources);
        }catch (Exception e){
            log.error(Constants.RES + ErrorCode.E_10001.getCode()+ "|/api/wx/advertise|首页轮播图展示", e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
