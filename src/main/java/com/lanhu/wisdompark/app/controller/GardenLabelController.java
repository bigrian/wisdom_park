package com.lanhu.wisdompark.app.controller;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.GardenLabel;
import com.lanhu.wisdompark.app.domain.WpResource;
import com.lanhu.wisdompark.app.service.GardenLabelService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/6/21
 */
@RestController
@Slf4j
@RequestMapping("/api/gardenLabel")
@Api(tags={"PC-园区标签接口服务"})
@CrossOrigin
public class GardenLabelController {

    @Autowired
    private GardenLabelService gardenLabelService;

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "查询园区所有标签图")
    public Result findAll(@RequestParam(defaultValue = "1") int pageNum,
                          @RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/gardenLabel/query|查询所有园区:pageNum={},pageSize={}",pageNum, pageSize);
        try {
            PageInfo<WpResource> pageInfos = gardenLabelService.findAllGardenLabel(pageNum,pageSize);
            return new Result(ErrorCode.SUCCESS,pageInfos);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/search")
    @ApiOperation(value = "查询某园区所有标签")
    public Result searchGardenLabel(@RequestParam String gardenId) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/gardenLabel/search|查询园区某园区所有标签:gardenId={}"+gardenId);
        try {
            List<GardenLabel> gardenLabels = gardenLabelService.searchGardenLabel(gardenId);
            return new Result(ErrorCode.SUCCESS,gardenLabels);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "根据id查询园区单个标签")
    public Result findGardenLabel(@RequestParam int id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/gardenLabel/find|根据id查询园区单个标签图:id={}");
        try {
            GardenLabel gardenLabel = gardenLabelService.findGardenLabel(id);
            return new Result(ErrorCode.SUCCESS,gardenLabel);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PutMapping(value = "/add")
    @ApiOperation(value = "添加园区标签信息")
    public Result addGardenLabel(@RequestBody GardenLabel gardenLabel) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/gardenLabel/add|添加园区信息:gardenLabel={}", gardenLabel);
        try {
            int i = gardenLabelService.addGardenLabel(gardenLabel);
            if (i>0){
                return new Result(ErrorCode.SUCCESS);
            }
            return new Result(ErrorCode.E_20029);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PostMapping(value = "/edit")
    @ApiOperation(value = "编辑园区标签信息")
    public Result editGardenLabel(@RequestBody GardenLabel gardenLabel) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/gardenLabel/edit|编辑园区标签图信息:gardenLabel={}", gardenLabel);
        try {
            int i = gardenLabelService.editGardenLabel(gardenLabel);
            if (i>0){
                return new Result(ErrorCode.SUCCESS);
            }
            return new Result(ErrorCode.E_20029);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @DeleteMapping(value = "/remove")
    @ApiOperation(value = "删除园区标签信息")
    public Result removeGardenLabel(@RequestParam int id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/gardenLabel/remove|删除园区标签图信息:id={}", id);
        try {
            int i = gardenLabelService.removeGardenLabel(id);
            if (i>0){
                return new Result(ErrorCode.SUCCESS);
            }
            return new Result(ErrorCode.E_20029);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
