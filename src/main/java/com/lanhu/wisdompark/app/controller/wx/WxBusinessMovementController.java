package com.lanhu.wisdompark.app.controller.wx;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.BusinessMovement;
import com.lanhu.wisdompark.app.service.wx.WxBusinessMovementService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author whongyu
 * @create by 2019/5/22
 */
@RestController
@Slf4j
@RequestMapping("/api/wx/business")
@Api(tags={"微信小程序-企业动态相关接口服务"})
@CrossOrigin
public class WxBusinessMovementController {

    @Autowired
    private WxBusinessMovementService wxBusinessMovementService;

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "查询该企业下的所有动态")
    public Result findAllCompanyByGardenId(@RequestParam String companyId,
                                           @RequestParam(defaultValue = "1") int pageNum,
                                           @RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/business/find|查询该园区下的所有入驻企业(包括logo):companyId={},pageNum={},pageSize={}",companyId, pageNum, pageSize);
        try {
            PageInfo<BusinessMovement> businessMovements = wxBusinessMovementService.findAllMovementByCompanyId(companyId,pageNum,pageSize);
            return new Result(ErrorCode.SUCCESS,businessMovements);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "根据企业动态id查看动态详情")
    public Result findAllCompanyByGardenId(@RequestParam String id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/business/query|根据企业动态id查看动态详情:id={}",id);
        try {
            BusinessMovement businessMovement = wxBusinessMovementService.findMovementById(id);
            return new Result(ErrorCode.SUCCESS,businessMovement);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
