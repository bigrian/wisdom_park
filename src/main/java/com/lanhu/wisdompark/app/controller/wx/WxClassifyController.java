package com.lanhu.wisdompark.app.controller.wx;

import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.Classify;
import com.lanhu.wisdompark.app.service.ClassifyService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/6/24
 */
@RestController
@Slf4j
@RequestMapping("/api/wx/classify")
@Api(tags={"微信小程序-分类相关接口服务"})
@CrossOrigin
public class WxClassifyController {

    @Autowired
    private ClassifyService classifyService;

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "查询某类型所有分类(二级)(type:1 业态分类)")
    public Result findClassifyGarde(@RequestParam int type) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/classify/find|查询某类型所有分类:type={}", type);
        try {
            List<Classify> classifyList = classifyService.findClassifyGarde(type);
            return new Result(ErrorCode.SUCCESS,classifyList);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
