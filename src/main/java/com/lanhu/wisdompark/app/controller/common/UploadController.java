package com.lanhu.wisdompark.app.controller.common;

import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.WpResource;
import com.lanhu.wisdompark.app.service.WpResourceService;
import com.lanhu.wisdompark.app.util.Result;
import com.lanhu.wisdompark.app.util.UploadUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;


@RestController
@Api(tags = {"公共接口服务-上传文件接口(文件,图片,视频等)"})
@RequestMapping(value = "/api/common/img")
@Slf4j
@CrossOrigin
public class UploadController {

	@Autowired
	private WpResourceService wpResourceService;

	@ApiOperation(value = "上传文件接口(0轮播图片及小视频,1园区vr视频,2园区pdf 文件,3微信小程序首页广告位轮播图 4楼层落位图 5园区标签图) 支持批量上传")
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	@ResponseBody
	public Result upload(@RequestParam MultipartFile[] files,@RequestParam int type){
		log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/common/img/uploadFile|上传图片接口,files: "+ files);
		try{
			UploadUtils upload = null;
			WpResource wpResource = null;
			List<WpResource> wpResources = new ArrayList<>();
			int count = 0;
			for (MultipartFile file :files){
				upload = new UploadUtils();
				String paths = upload.uploadBlob(file, "parkfile");//parkfile 为blob容器名称
				if (!"".equals(paths)){
					wpResource = new WpResource();
					wpResource.setType(type);
					wpResource.setPath(paths);
					wpResource.setSort(count++);
					wpResourceService.addWpResource(wpResource);
					wpResources.add(wpResource);

				}else{
					return new Result(ErrorCode.E_10001);
				}
			}
			log.info(Constants.RES + ErrorCode.SUCCESS.getCode() + "|api/common/img/uploadFile|上传图片接口,wpResources: " + wpResources);
			return new Result(ErrorCode.SUCCESS,wpResources);
		}catch (Exception e){
			log.error(Constants.RES + ErrorCode.E_10001.getCode()+ "|api/common/img/uploadFile|上传图片接口报错", e);
			return new Result(ErrorCode.E_10001);
		}
	}

	@ApiOperation(value = "删除文件接口")
	@DeleteMapping(value = "/delete")
	@ResponseBody
	public Result delete(@RequestParam int id){
		log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/common/img/delete|删除文件接口,id: "+ id);
		try{
			wpResourceService.deleteResource(id);
			return new Result(ErrorCode.SUCCESS);
		}catch (Exception e){
			log.error(Constants.RES + ErrorCode.E_10001.getCode()+ "|api/common/img/delete|删除文件接口报错", e);
			return new Result(ErrorCode.E_10001);
		}
	}

}
