package com.lanhu.wisdompark.app.controller.wx;

import com.alibaba.fastjson.JSON;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.WeChatRes;
import com.lanhu.wisdompark.app.domain.WxAppUser;
import com.lanhu.wisdompark.app.service.GardenInfoService;
import com.lanhu.wisdompark.app.service.wx.WxUserService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;

import static java.security.KeyRep.Type.SECRET;

/**
 * @author whongyu
 * @create by 2019/5/22
 */
@RestController
@Slf4j
@RequestMapping("/api/wx/user")
@Api(tags={"微信小程序-用户相关接口服务"})
@CrossOrigin
public class WxUserController {

//    private static final String APPID = "wx78783933469448f8";
//    private static final String SECRET = "44adbf293c840806dc75f1a594ac1ca6";
    private static final String URL = "https://api.weixin.qq.com/sns/jscode2session?appid=";

    @Autowired
    private WxUserService wxUserService;
    @Autowired
    private GardenInfoService gardenInfoService;

    /**
     * 微信小程序用户首次授权登录
     * @param js_code
     * @return
     */
    @GetMapping(value = "/visit")
    @ApiOperation(value = "用户首次授权登录")
    public Result findByWxUserId(@RequestParam(required = true) String js_code,@RequestParam String appId, HttpServletResponse response) {
        //ajax实现跨域请求
        response.setHeader("Access-Control-Allow-Origin", "*");

        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/user/visit|微信小程序用户首次授权登录接口:js_code={}", js_code);
        try {
            String secret = gardenInfoService.findSecret(appId);
            //微信的接口
            String url = URL + appId + "&secret="+secret+"&js_code="+ js_code +"&grant_type=authorization_code";
            //log.info("调用微信授权接口URL: " + url);
            RestTemplate restTemplate = new RestTemplate();
            //进行网络请求,访问url接口
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
            //根据返回值进行后续操作
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                log.info("调用微信授权接口返回正常！");
                String sessionData = responseEntity.getBody();
                log.info("Boyd转化sessionData:"+sessionData);
                //解析从微信服务器获得的openid和session_key;
                WeChatRes weChatRes = JSON.parseObject(sessionData, WeChatRes.class);
                log.info("从微信获取到的openid=" + weChatRes.getOpenid());
                //首先根据openid去表中查询，查询到就不再存储
                WxAppUser originWxUser = wxUserService.findByOpenId(weChatRes.getOpenid());
                if (originWxUser != null) {
                    if (weChatRes.getSession_key() != null) { //微信小程序再次调用wx.login时要更新sessionKEY
                        originWxUser.setSessionKey(weChatRes.getSession_key());
                        wxUserService.update(originWxUser);
                    }
                    return new Result(ErrorCode.SUCCESS, originWxUser);
                } else {
                    if (weChatRes != null && weChatRes.getOpenid() != null) {
                        WxAppUser wxUser = new WxAppUser();
                        wxUser.setOpenId(weChatRes.getOpenid());
                        wxUser.setSessionKey(weChatRes.getSession_key());
                        wxUser.setUnionid(weChatRes.getUnionid());
                        wxUser.setStatus(0);
                        wxUserService.add(wxUser);
                        wxUser.setSessionKey(null);//不传给调用方
                        return new Result(ErrorCode.SUCCESS, wxUser);
                    }
                }
            } else{
                log.error("调用微信授权接口返回异常！");
            }
            return new Result(ErrorCode.E_10001);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ApiOperation(value = "授权成功回调接口/修改个人资料接口")
    @PostMapping("/update")
    public Result getinfo(@RequestBody WxAppUser wxAppUser){
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/user/update|授权成功回调接口/修改个人资料接口:wxAppUser={}",wxAppUser);
        try{
            wxUserService.update(wxAppUser);
            return new Result(ErrorCode.SUCCESS);
        }catch (Exception e){
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @ApiOperation(value = "微信小程序获取个人资料接口")
    @GetMapping("/find")
    public Result getinfo(@RequestParam int id){
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/user/find|微信小程序用户获取个人资料接口:id={}", id);
        try{
            WxAppUser source = wxUserService.findById(id);
            return new Result(ErrorCode.SUCCESS,source);
        }catch (Exception e){
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
