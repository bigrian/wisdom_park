package com.lanhu.wisdompark.app.controller;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.PolicyNews;
import com.lanhu.wisdompark.app.service.PolicyNewsService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author whongyu
 * @create by 2019/6/26
 */
@RestController
@Slf4j
@RequestMapping("/api/policyNews")
@Api(tags={"PC-新闻政策相关接口服务"})
@CrossOrigin
public class PolicyNewsController {

    @Autowired
    private PolicyNewsService policyNewsService;

    @ResponseBody
    @PutMapping(value = "/add")
    @ApiOperation(value = "创建新闻政策相关接口")
    public Result addPolicyNews(@RequestBody PolicyNews policyNews) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/policyNews/add|创建N级菜单接口 policyNews={}",policyNews);
        try {
            int i = policyNewsService.addPolicyNews(policyNews);
            if(i>0){
                return new Result(ErrorCode.SUCCESS);
            }else {
                return new Result(ErrorCode.E_20029);
            }
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "查询某园区新闻政策相关接口")
    public Result findPolicyNews(@RequestParam String gardenId,@RequestParam int pageNum,@RequestParam int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/policyNews/find|查询某园区新闻政策相关接口 gardenId={},pageNum={},pageSize={}",gardenId,pageNum,pageSize);
        try {
            PageInfo<PolicyNews> policyNewsList = policyNewsService.findPolicyNews(gardenId,pageNum,pageSize);
            return new Result(ErrorCode.SUCCESS,policyNewsList);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PostMapping(value = "/edit")
    @ApiOperation(value = "编辑园区新闻政策相关接口")
    public Result editPolicyNews(@RequestParam PolicyNews policyNews) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/policyNews/edit|编辑园区新闻政策相关接口 policyNews={}",policyNews);
        try {
            int i = policyNewsService.editPolicyNews(policyNews);
            if(i>0){
                return new Result(ErrorCode.SUCCESS);
            }else {
                return new Result(ErrorCode.E_20029);
            }
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "根据id查询园区新闻政策接口")
    public Result queryPolicyNews(@RequestParam int id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/policyNews/query|根据id查询园区新闻政策接口 id={}",id);
        try {
            PolicyNews policyNews = policyNewsService.queryPolicyNews(id);
            return new Result(ErrorCode.SUCCESS,policyNews);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
