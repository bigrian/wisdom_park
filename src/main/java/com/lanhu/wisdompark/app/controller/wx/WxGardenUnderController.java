package com.lanhu.wisdompark.app.controller.wx;

import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.EnterAudit;
import com.lanhu.wisdompark.app.domain.GardenLabel;
import com.lanhu.wisdompark.app.domain.GardenUnit;
import com.lanhu.wisdompark.app.service.wx.WxEnterAuditService;
import com.lanhu.wisdompark.app.service.wx.WxGardenInfoService;
import com.lanhu.wisdompark.app.service.wx.WxGardenLabelService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/25
 */
@RestController
@Slf4j
@RequestMapping("/api/wx/garden/under")
@Api(tags={"微信小程序-园区下—相关接口服务"})
@CrossOrigin
public class WxGardenUnderController {

    @Autowired
    private WxGardenInfoService wxGardenInfoService;
    @Autowired
    private WxGardenLabelService wxGardenLabelService;
    @Autowired
    private WxEnterAuditService wxEnterAuditService;

    @ResponseBody
    @PostMapping(value = "/counts")
    @ApiOperation(value = "园区下-首页访问量等统计接口")
    public Result visitCounts(@RequestParam String gardenId, @RequestParam int userId) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/garden/under/counts|园区下-首页访问量等统计接口:gardenId={},userId={}", gardenId,userId);
        try {
            Map<String,Integer> map = wxGardenInfoService.visitCounts(gardenId,userId);
            return new Result(ErrorCode.SUCCESS,map);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/list")
    @ApiOperation(value = "查询该所有园区户型接口(包含访问人次)")
    public Result findAllGardenUnit(@RequestParam String gardenId) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/garden/under/list|园区下-查询所有园区户型接口:garendId={}", gardenId);
        try {
            List<GardenUnit> gardenUnits = wxGardenInfoService.findAllGardenUnit(gardenId);
            return new Result(ErrorCode.SUCCESS,gardenUnits);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "根据id查询园区户型接口")
    public Result findGardenUnit(@RequestParam String gardenId,@RequestParam int gardenUnitId,@RequestParam int wxAppUserId) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/garden/under/find|园区下-根据id查询园区户型接口:gardenId={},gardenUnitId={},wxAppUserId={}", gardenId,gardenUnitId,wxAppUserId);
        try {
            GardenUnit gardenUnit = wxGardenInfoService.findGardenUnit(gardenId,gardenUnitId,wxAppUserId);
            return new Result(ErrorCode.SUCCESS,gardenUnit);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/label")
    @ApiOperation(value = "查询该园区所有标签")
    public Result findLabel(@RequestParam String gardenId) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/garden/under/label|园区下-查询该园区所有标签:gardenId={}", gardenId);
        try {
            List<GardenLabel> gardenLabels = wxGardenLabelService.findAllLabel(gardenId);
            return new Result(ErrorCode.SUCCESS,gardenLabels);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PutMapping(value = "/enter")
    @ApiOperation(value = "申请入驻接口(预约)")
    public Result enterGarden(@RequestBody EnterAudit enterAudit) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/garden/under/enter|园区下-申请入驻接口(预约):enterAudit={}", enterAudit);
        try {
            int i = wxEnterAuditService.enterGarden(enterAudit);
            if (i == -1){
                return new Result(ErrorCode.E_20018);
            }else if(i >0){
                return new Result(ErrorCode.SUCCESS);
            }
            return new Result(ErrorCode.SUCCESS);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "用户查询预约详情")
    public Result queryEnter(@RequestParam int userId) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/garden/under/query|园区下-用户查询预约详情(预约):userId={}", userId);
        try {
            List<EnterAudit> enterAudits = wxEnterAuditService.queryEnter(userId);
            return new Result(ErrorCode.SUCCESS,enterAudits);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }


}
