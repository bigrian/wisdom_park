package com.lanhu.wisdompark.app.controller;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.Area;
import com.lanhu.wisdompark.app.service.AreaService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *  区域管理控制层
 * @create by 2019/6/28
 */
@Slf4j
@Controller
@RequestMapping(value = "/api/area")
@Api(tags={"区域相关接口"})
public class AreaController {

    @Autowired
    private AreaService areaService;

    @ResponseBody
    @GetMapping(value = "/all")
    @ApiOperation(value = "查询所有区域列表接口")
    public Result findAll () {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/area/all|查询所有区域列表接口:");
        try {
            List<Area> areaList = areaService.findAll();
            return new Result(ErrorCode.SUCCESS, areaList);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/list")
    @ApiOperation(value = "根据区域上级code查询区域列表接口")
    public Result findAll (@RequestParam(defaultValue = "") String parentCode) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/area/list|根据区域上级code查询区域列表接口:parentCode={}", parentCode);
        try {
            List<Area> areaList = areaService.findAreaByParent(parentCode);
            return new Result(ErrorCode.SUCCESS, areaList);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/name")
    @ApiOperation(value = "根据省市区code查询对应的名称")
    public Result findNameByCode (@RequestParam(defaultValue = "-1") String province,
                                  @RequestParam(defaultValue = "-1") String city,
                                  @RequestParam(defaultValue = "-1") String county,
                                  @RequestParam(defaultValue = "") String septor) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/area/name|根据省市区code查询对应的名称:province:{},city:{},county:{},septor:{}", province, city, county,septor);
        try {
            String areaName = areaService.findNameByCode(septor, province, city, county);
            return new Result(ErrorCode.SUCCESS, areaName);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "条件查询(区域名称，区域编码)区域列表接口")
    public Result queryArea (@RequestParam(defaultValue = "") String code,
                             @RequestParam(defaultValue = "") String name,
                             @RequestParam(defaultValue = "") String parentCode,
                             @RequestParam(defaultValue = "1") int pageNum,
                             @RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/area/query|条件查询(区域名称，区域编码)区域列表接口:code:{},name:{},parentCode:{},pageNum:{},pageSize:{}",
                code, name, parentCode, pageNum, pageSize);
        try {
            PageInfo<Area> areas = areaService.queryArea(code, name, parentCode, pageNum, pageSize);
            return new Result(ErrorCode.SUCCESS, areas);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/pagelist")
    @ApiOperation(value = "根据区域上级code查询区域列表接口(分页版）")
    public Result findAll (@RequestParam(defaultValue = "") String parentCode,
                           @RequestParam(defaultValue = "1") int pageNum,
                           @RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/area/query|根据区域上级code查询区域列表接口(分页版）:parentCode:{},pageNum:{},pageSize:{}", parentCode, pageNum, pageSize);
        try {
            PageInfo<Area> areaList = areaService.findAreaByParent(parentCode, pageNum, pageSize);
            return new Result(ErrorCode.SUCCESS, areaList);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

}
