package com.lanhu.wisdompark.app.controller;

import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.Classify;
import com.lanhu.wisdompark.app.service.ClassifyService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/6/22
 */
@RestController
@Slf4j
@RequestMapping("/api/classify")
@Api(tags={"PC-分类相关接口服务"})
@CrossOrigin
public class ClassifyController {

    @Autowired
    private ClassifyService classifyService;

    @ResponseBody
    @PutMapping(value = "/add")
    @ApiOperation(value = "添加分类信息(type: 1 行业分类)")
    public Result addClassify(@RequestBody Classify classify) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/classify/add|添加分类信息:classify={}", classify);
        try {
            int i = classifyService.addClassify(classify);
            if (i>0){
                return new Result(ErrorCode.SUCCESS);
            }
            return new Result(ErrorCode.E_20029);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "查询分类信息(type: 1 行业分类)")
    public Result findClassify(@RequestParam int type) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/classify/find|查询分类信息:type={}", type);
        try {
            List<Classify> classifies = classifyService.findClassify(type);
            return new Result(ErrorCode.SUCCESS,classifies);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "查询父节点下的分类信息(type: 1 行业分类)")
    public Result queryClassify(@RequestParam int parentId) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/classify/query|查询父节点下的分类信息:parentId={}", parentId);
        try {
            List<Classify> classifies = classifyService.queryClassify(parentId);
            return new Result(ErrorCode.SUCCESS,classifies);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/search")
    @ApiOperation(value = "根据id查询分类信息")
    public Result searchClassify(@RequestParam int id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/classify/search|根据id查询分类信息:id={}", id);
        try {
            Classify classify = classifyService.searchClassify(id);
            return new Result(ErrorCode.SUCCESS,classify);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PostMapping(value = "/edit")
    @ApiOperation(value = "编辑分类信息(type 不许修改)")
    public Result editClassify(@RequestBody Classify classify) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/classify/edit|编辑分类信息:classify={}", classify);
        try {
            int i = classifyService.editClassify(classify);
            if (i>0){
                return new Result(ErrorCode.SUCCESS);
            }
            return new Result(ErrorCode.E_20029);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PutMapping(value = "/remove")
    @ApiOperation(value = "删除分类信息")
    public Result removeClassify(@RequestParam int id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/classify/remove|删除分类信息:id={}", id);
        try {
            int i = classifyService.removeClassify(id);
            if (i>0){
                return new Result(ErrorCode.SUCCESS);
            }
            return new Result(ErrorCode.E_20029);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

}
