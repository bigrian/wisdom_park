package com.lanhu.wisdompark.app.controller;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.GardenInfo;
import com.lanhu.wisdompark.app.domain.User;
import com.lanhu.wisdompark.app.service.GardenInfoService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * @author whongyu
 * @create by 2019/5/21
 */
@RestController
@Slf4j
@RequestMapping("/api/garden")
@Api(tags={"PC-园区相关接口服务"})
@CrossOrigin
public class GardenInfoController {

    @Autowired
    private GardenInfoService gardenInfoService;

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "查询该用户所有园区")
    public Result findAll(@RequestParam int userId,
                          @RequestParam(defaultValue = "1") int pageNum,
                          @RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/list|查询所有园区:userId={},pageNum={},pageSize={}",userId, pageNum, pageSize);
        try {
            PageInfo<GardenInfo> gardenInfos = gardenInfoService.findGardenByUser(userId,pageNum,pageSize);
            return new Result(ErrorCode.SUCCESS,gardenInfos);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PutMapping(value = "/add")
    @ApiOperation(value = "添加园区信息(UserId 必传)")
    public Result addGardenInfo(@RequestBody GardenInfo gardenInfo, HttpSession session) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/add|添加园区信息:gardenInfo={},resources", gardenInfo);
        try {
            //初始化 根据登录人 区域判定
            User user = (User)session.getAttribute("user");
            int i=0;
            if(user.getType() == 2  || user.getType() == 1 ){//不是admin 和园区拥有者
                i = gardenInfoService.addGardenInfo(gardenInfo);
            }else {
                return new Result(ErrorCode.E_20017);//无权限
            }
            if (i>0){
                return new Result(ErrorCode.SUCCESS,gardenInfo);
            }else if(i == -1){
                return new Result(ErrorCode.E_20032);
            }
            return new Result(ErrorCode.E_20029);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    /*@ApiOperation(value = "园区pdf文件上传接口")
    @RequestMapping(value = "/uploadPdf", method = RequestMethod.POST)
    @ResponseBody
    public Result upload(@RequestParam MultipartFile file,@RequestParam String gardenId){
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/api/garden/uploadPdf|园区pdf文件上传接口,file: "+ file);
        try{
            UploadUtils upload = new UploadUtils();
            String paths = upload.uploadBlob(file, "parkfile");//parkfile 为blob容器名称
            GardenInfo gardenInfo = gardenInfoService.queryGardenById(gardenId);
            gardenInfo.setPdfPath(paths);
            gardenInfoService.saveGarden(gardenInfo);
            log.info(Constants.RES + ErrorCode.SUCCESS.getCode() + "|api/api/garden/uploadPdf|园区pdf文件上传接口: ");
            return new Result(ErrorCode.SUCCESS,paths);
        }catch (Exception e){
            log.error(Constants.RES + ErrorCode.E_10001.getCode()+ "|api/api/garden/uploadPdf|园区pdf文件上传接口", e);
            return new Result(ErrorCode.E_10001);
        }
    }*/

    @ResponseBody
    @PostMapping(value = "/update")
    @ApiOperation(value = "修改园区信息(id必传)")
    public Result UpdateGardenInfo(@RequestBody GardenInfo gardenInfo) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/update|修改园区信息:gardenInfo={}", gardenInfo);
        try {
            int i = gardenInfoService.UpdateGardenInfo(gardenInfo);
            if (i>0){
                return new Result(ErrorCode.SUCCESS);
            }
            return new Result(ErrorCode.E_20029);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "根据id查询园区信息")
    public Result findGardenById(String id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/garden/find|根据id查询园区信息:id={}",id);
        try {
            GardenInfo gardenInfo = gardenInfoService.findGardenById(id);
            return new Result(ErrorCode.SUCCESS,gardenInfo);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
