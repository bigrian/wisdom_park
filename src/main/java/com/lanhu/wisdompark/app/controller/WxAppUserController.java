package com.lanhu.wisdompark.app.controller;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.WxAppUser;
import com.lanhu.wisdompark.app.service.WxAppUserService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/23
 */
@RestController
@Slf4j
@RequestMapping("/api/wxAppUser")
@Api(tags={"PC-微信与APP用户相关接口服务"})
@CrossOrigin
public class WxAppUserController {

    @Autowired
    private WxAppUserService wxAppUserService;


    @ResponseBody
    @GetMapping(value = "/list")
    @ApiOperation(value = "查询所有查询wx与app用户列表")
    public Result DeleteGardenInfo(@RequestParam(defaultValue = "1") int pageNum,@RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/user/list|查询所有用户列表:");
        try {
            PageInfo<WxAppUser> users = wxAppUserService.findAllUser(pageNum, pageSize);
            return new Result(ErrorCode.SUCCESS,users);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "多条件查询wx与app用户")
    public Result findMultiple(@RequestParam(defaultValue = "-1") String province,
                               @RequestParam(defaultValue = "-1") String city,
                               @RequestParam(defaultValue = "-1") String county,
                               @RequestParam(defaultValue = "-1") String userName,
                               @RequestParam(defaultValue = "-1") String mobile) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/user/list|查询所有用户列表:");
        try {
            List<WxAppUser> users = wxAppUserService.findMultiple(province, city, county, userName, mobile);
            return new Result(ErrorCode.SUCCESS,users);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "根据id查询wx与app用户信息")
    public Result updateWxAppUser(@RequestParam int id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/user/query|根据id查询用户信息用户:");
        try {
            WxAppUser wxAppUser = wxAppUserService.findWxAppUserById(id);
            return new Result(ErrorCode.SUCCESS,wxAppUser);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PostMapping(value = "/update")
    @ApiOperation(value = "编辑wx与app用户信息")
    public Result updateWxAppUser(@RequestBody WxAppUser wxAppUser) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/user/update|编辑用户信息用户:");
        try {
            wxAppUserService.updateWxAppUser(wxAppUser);
            return new Result(ErrorCode.SUCCESS);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

}
