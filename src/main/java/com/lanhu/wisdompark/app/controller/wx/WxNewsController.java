package com.lanhu.wisdompark.app.controller.wx;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.PolicyNews;
import com.lanhu.wisdompark.app.service.wx.WxPolicyNewsService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author whongyu
 * @create by 2019/6/25
 */
@RestController
@Slf4j
@RequestMapping("/api/wx/news")
@Api(tags={"微信小程序-政策新闻相关接口服务"})
@CrossOrigin
public class WxNewsController {

    @Autowired
    private WxPolicyNewsService wxPolicyNewsService;

    @ResponseBody
    @PostMapping(value = "/find")
    @ApiOperation(value = "某园区新闻查询接口")
    public Result findNews(@RequestParam String gardenId,@RequestParam int pageNum,@RequestParam int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/news/find|某园区新闻查询接口:gardenId={},pageNum={},pageSize={}", gardenId,pageNum,pageSize);
        try {
            PageInfo<PolicyNews> policyNews = wxPolicyNewsService.findNews(gardenId,pageNum,pageSize);
            return new Result(ErrorCode.SUCCESS,policyNews);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
