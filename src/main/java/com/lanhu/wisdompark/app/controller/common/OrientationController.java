package com.lanhu.wisdompark.app.controller.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


/**
 * @author whongyu
 * @create by 2019/5/22
 */
@RestController
@RequestMapping(value = "/api/common/map")
@Slf4j
@Api(tags = {"公共接口服务-腾讯地图相关"})
@CrossOrigin
public class OrientationController {

    private String key="2KFBZ-3QXCJ-LFBF7-FJMDV-NQLCQ-OZFZ4";//腾讯地图  key

    @ResponseBody
    @GetMapping(value = "/getAdress")
    @ApiOperation(value = "腾讯-根据经纬度获取地址")
    public Result getAdress(@RequestParam double longitude,@RequestParam double latitude){
        log.info("腾讯-根据经纬度获取地址方法调用成功!"+longitude+","+latitude);
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://apis.map.qq.com/ws/geocoder/v1/?location="+latitude+","+longitude+"&key="+key+"&get_poi=1";
        log.info("腾讯-根据经纬度获取地址url:"+url);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        JSONObject object = null;
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            String body = responseEntity.getBody();
            JSONObject obj = JSON.parseObject(body);
            object = obj.getJSONObject("result");
            log.info("腾讯-根据经纬度获取地址为:" + object);
        }
        return new Result(ErrorCode.SUCCESS,object);
    }

    @ResponseBody
    @GetMapping(value = "/getLongLat")
    @ApiOperation(value = "腾讯-根据地址获取经纬度")
    public Result getLongLat(@RequestParam String address){
        log.info("腾讯-根据地址获取经纬度调用成功!"+address);
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://apis.map.qq.com/ws/geocoder/v1/?address="+address+"&key="+key;//定位到门址
        log.info("腾讯-根据地址获取经纬度url:"+url);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        JSONObject object = null;
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            String body = responseEntity.getBody();
            JSONObject obj = JSON.parseObject(body);
            object = obj.getJSONObject("result").getJSONObject("location");
            log.info("腾讯-根据地址获取经纬度为:" + object);
        }
        return  new Result(ErrorCode.SUCCESS,object);
    }
}
