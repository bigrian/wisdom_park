package com.lanhu.wisdompark.app.controller;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.User;
import com.lanhu.wisdompark.app.service.SysUserService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;


/**
 * @author whongyu
 * @create by 2019/5/23
 */
@RestController
@Slf4j
@RequestMapping("/api/user")
@Api(tags={"PC-系统用户相关接口服务"})
@CrossOrigin
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    @ResponseBody
    @PostMapping(value = "/login")
    @ApiOperation(value = "用户登录后台系统接口")
    public Result login(@RequestParam String userName, @RequestParam String password, HttpSession session) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/user/login|用户登录后台系统接口:");
        try {
            Result result = sysUserService.login(userName, password);
            User user = (User) result.getResult_data();
            session.setAttribute("user",user);
            return result;
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/list")
    @ApiOperation(value = "查询所有用户接口")
    public Result findAll(@RequestParam(defaultValue = "1") int pageNum,@RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/user/list|查询所有用户接口:");
        try {
            PageInfo<User> users = sysUserService.findAll(pageNum,pageSize);
            return new Result(ErrorCode.SUCCESS,users);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PostMapping(value = "/update")
    @ApiOperation(value = "编辑系统用户接口")
    public Result updateUser(@RequestBody User user) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/user/update|编辑系统用户接口 user={}:"+user);
        try {
            int i = sysUserService.updateUser(user);
            if(i>0){
                return new Result(ErrorCode.SUCCESS);
            }
            return new Result(ErrorCode.E_20025);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PostMapping(value = "/modify")
    @ApiOperation(value = "用户修改密码接口")
    public Result updatePass (@RequestParam int id,@RequestParam String oldPass,@RequestParam String newPass) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/user/modify|用户修改密码接口:id={},oldPass={},newPass={}",id,oldPass,newPass);
        try {
            return sysUserService.changePassword(id, oldPass, newPass);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);

        }
    }

    @ResponseBody
    @PutMapping(value = "/add")
    @ApiOperation(value = "添加系统用户(type 1:admin 2:园区拥有者(运营者) 3:单个园区(管理者)")
    public Result addUser (@RequestBody User user) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/user/add|添加系统用户:user={}",user);
        try {
            int i = sysUserService.addUser(user);
            if(i>0){
                return new Result(ErrorCode.SUCCESS);
            }
            if (i == -1){
                return new Result(ErrorCode.E_20006);
            }
            return new Result(ErrorCode.E_20029);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "根据id查询用户")
    public Result queryUser (@RequestParam int id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/user/query|根据id查询用户:id={}",id);
        try {
            User user = sysUserService.queryUser(id);
            return new Result(ErrorCode.SUCCESS,user);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
