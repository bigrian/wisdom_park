package com.lanhu.wisdompark.app.controller;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.CompanyInfo;
import com.lanhu.wisdompark.app.service.CompanyInfoService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author whongyu
 * @create by 2019/6/3
 */
@RestController
@Slf4j
@RequestMapping("/api/company")
@Api(tags={"PC-企业相关接口服务"})
@CrossOrigin
public class CompanyInfoController {

    @Autowired
    private CompanyInfoService companyInfoService;

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "查询园区入驻企业接口")
    public Result findCompanyInfo(@RequestParam String gardenId,
                                  @RequestParam(defaultValue = "1") int pageNum,
                                  @RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/company/find|查询园区入驻企业接口 gardenId={},pageNum={},pageSize={}"+gardenId,pageNum,pageSize);
        try {
           PageInfo<CompanyInfo> companyInfos = companyInfoService.findCompanyInfo(gardenId,pageNum,pageSize);
            return new Result(ErrorCode.SUCCESS,companyInfos);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "根据id查询入驻企业")
    public Result queryCompanyInfo(@RequestParam String id) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/company/query|查询入驻企业接口:id={}"+id);
        try {
           CompanyInfo companyInfo = companyInfoService.queryCompanyInfo(id);
            return new Result(ErrorCode.SUCCESS,companyInfo);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PostMapping(value = "/update")
    @ApiOperation(value = "入驻企业编辑接口")
    public Result updateCompanyInfo(@RequestBody CompanyInfo companyInfo) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/company/update|入驻企业编辑接口:companyInfo={}",companyInfo);
        try {
            int i = companyInfoService.updateCompanyInfo(companyInfo);
            if(-1 == i){
                return new Result(ErrorCode.E_20021);
            }else {
                return new Result(ErrorCode.SUCCESS);
            }
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @PostMapping(value = "/add")
    @ApiOperation(value = "入驻企业添加接口")
    public Result addCompanyInfo(@RequestBody CompanyInfo companyInfo) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/company/add|入驻企业添加接口:companyInfo={}",companyInfo);
        try {
            int i = companyInfoService.addCompanyInfo(companyInfo);
            if(-1 == i){
                return new Result(ErrorCode.E_20021);
            }else {
                return new Result(ErrorCode.SUCCESS);
            }
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

}
