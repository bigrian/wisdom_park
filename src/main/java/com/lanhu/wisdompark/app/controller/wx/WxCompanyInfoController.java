package com.lanhu.wisdompark.app.controller.wx;

import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.Constants;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.CompanyInfo;
import com.lanhu.wisdompark.app.service.wx.WxCompanyInfoService;
import com.lanhu.wisdompark.app.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author whongyu
 * @create by 2019/5/21
 */
@RestController
@Slf4j
@RequestMapping("/api/wx/company")
@Api(tags={"微信小程序-入驻企业相关接口服务"})
@CrossOrigin
public class WxCompanyInfoController {

    @Autowired
    private WxCompanyInfoService wxCompanyInfoService;

    @ResponseBody
    @GetMapping(value = "/find")
    @ApiOperation(value = "查询该园区下的所有入驻企业(包括logo)")
    public Result findAllCompanyByGardenId(@RequestParam String gardenId,
                                           @RequestParam(defaultValue = "1") int pageNum,
                                           @RequestParam(defaultValue = "10") int pageSize) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/company/find|查询该园区下的所有入驻企业(包括logo):gardenId={},pageNum={},pageSize={}",gardenId, pageNum, pageSize);
        try {
            PageInfo<CompanyInfo> companys = wxCompanyInfoService.findAllCompanyByGardenId(gardenId,pageNum,pageSize);
            return new Result(ErrorCode.SUCCESS,companys);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }

    @ResponseBody
    @GetMapping(value = "/query")
    @ApiOperation(value = "根据id查询企业相关信息")
    public Result findAllCompanyByGardenId(@RequestParam String companyId) {
        log.info(Constants.REQ + ErrorCode.SUCCESS.getCode() + "|api/wx/company/query|根据id查询企业相关信息:companyId={}",companyId);
        try {
            CompanyInfo company = wxCompanyInfoService.findCompanyByCompanyId(companyId);
            return new Result(ErrorCode.SUCCESS,company);
        } catch (Exception e) {
            log.error(Constants.RES + ErrorCode.E_10001.getCode(), e);
            return new Result(ErrorCode.E_10001);
        }
    }
}
