package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.VisitGarden;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @author whongyu
 * @create by 2019/5/21
 */
@Repository
public interface VisitGardenMapper {

    @Insert("insert into wp_visit_garden(`id`, `wx_app_user_id`, `garden_id`) values(#{id}, #{wxAppUserId}, #{gardenId})")
    int addVisitGarden(VisitGarden visitGarden);

    @Select(" select count(*) as counts from wp_visit_garden where garden_id = #{gardenId} ")
    int countVisit(String gardenId);
}
