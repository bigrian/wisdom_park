package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.WpResource;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/21
 */
@Repository
public interface ResourceMapper {

    @Select(" select `id`,`resource_id`,`path`,`type`,`sort`,`create_time` from wp_resource where resource_id =#{resourceId} and type =#{type} " +
            " order by sort asc")
    List<WpResource> getResource(String resourceId, int type);

    @Insert("insert into wp_resource( `resource_id`, `path`, `type`, `sort`) values(#{resourceId}, #{path}, #{type}, #{sort})")
    @Options(keyProperty = "id",useGeneratedKeys = true)
    int addWpResource(WpResource wpResource);

    @Delete("delete from wp_resource where id = #{id}")
    int deleteResource(int resourceId);

    @Update("<script>" +
            "update wp_resource " +
            "<trim prefix=\"set\" suffixOverrides=\",\">" +
            "   <trim prefix=\"resource_id =case\" suffix=\"end,\">" +
            "       <foreach collection=\"list\" item=\"item\" index=\"index\">" +
            "           <if test=\"item.resourceId !=null\"> when id=#{item.id} then #{item.resourceId} </if>" +
            "       </foreach>" +
            "   </trim>" +
            "</trim>"+
            "<where>" +
            " id in "+
            "<foreach collection=\"list\" item=\"item\" open=\"(\" separator=\",\" close=\")\">" +
            " #{item.id} " +
            "</foreach>"+
            "</where>" +
            "</script>")
    int batchUpdate(List<WpResource> resources);

    @Select("<script>" +
            "select `id`,`resource_id`,`path`,`type`,`create_time` from wp_resource" +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"resourceId != null and resourceId != ''\"> and resource_id = #{resourceId} </if>" +
            "<if test=\"path != null and path != ''\"> and path = #{path} </if>" +
            "<if test=\"type != null and type != ''\"> and type = #{type} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "</where>" +
            "<choose>" +
            "   <when test=\"sort != null and sort.trim() != ''\">" +
            "        order by ${sort} asc" +
            "   </when>" +
            "   <otherwise>" +
            "        order by id desc" +
            "   </otherwise>" +
            "</choose>" +
            "</script>")
    List<WpResource> findAll(Map<String, Object> map);
}
