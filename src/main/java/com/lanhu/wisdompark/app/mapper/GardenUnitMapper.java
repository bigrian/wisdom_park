package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.GardenUnit;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/25
 */
@Repository
public interface GardenUnitMapper {

    @Select("<script>" +
            "select `id`,`garden_id`,`garden_unit_num`,`describe`,`path`,`floor_plan`,`area`,`bitmap_path`,`sort`,`status`,`create_time` from wp_garden_unit " +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"gardenId != null and gardenId != ''\"> and garden_id = #{gardenId} </if>" +
            "<if test=\"gardenUnitNum != null and gardenUnitNum != ''\"> and garden_unit_num = #{gardenUnitNum} </if>" +
            "<if test=\"describe != null and describe != ''\"> and describe = #{describe} </if>" +
            "<if test=\"path != null and path != ''\"> and path = #{path} </if>" +
            "<if test=\"status != null and status != ''\"> and status = #{status} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "</where>" +
            "<choose>" +
            "  <when test=\"sort != null and sort.trim() != ''\">" +
            "       order by ${sort} ${order}" +
            "  </when>" +
            "<otherwise>" +
            "       order by id desc" +
            "</otherwise>" +
            "</choose>" +
            "</script>")
    @ResultType(GardenUnit.class)
    List<GardenUnit> findAll(Map<String, Object> map);

    @Select("<script>" +
            "select `id`,`garden_id`,`garden_unit_num`,`describe`,`path`,`floor_plan`,`area`,`bitmap_path`,`sort`,`status`,`create_time` from wp_garden_unit " +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"gardenId != null and gardenId != ''\"> and garden_id = #{gardenId} </if>" +
            "<if test=\"gardenUnitNum != null and gardenUnitNum != ''\"> and garden_unit_num like concat('%',#{gardenUnitNum},'%') </if>" +
            "<if test=\"describe != null and describe != ''\"> and describe like concat('%',#{describe},'%') </if>" +
            "<if test=\"path != null and path != ''\"> and path = #{path} </if>" +
            "<if test=\"status != null and status != ''\"> and status = #{status} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "</where>" +
            "<choose>" +
            "  <when test=\"sort != null and sort.trim() != ''\">" +
            "       order by ${sort} ${order}" +
            "  </when>" +
            "<otherwise>" +
            "       order by id desc" +
            "</otherwise>" +
            "</choose>" +
            "</script>")
    @ResultType(GardenUnit.class)
    List<GardenUnit> searchGarden(GardenUnit gardenUnit);

    @Insert("insert into wp_garden_unit(`garden_id`, `garden_unit_num`, `describe`, `path`, `floor_plan`,`area`,`bitmap_path`,`sort`) values(#{gardenId}, #{gardenUnitNum}, #{describe}, #{path}, #{floorPlan}, #{area}, #{bitmapPath}, #{sort})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    int add(GardenUnit gardenUnit);

    @Select("select `id`,`garden_id`,`garden_unit_num`,`describe`,`path`,`floor_plan`,`area`,`bitmap_path`,`sort`,`status`,`create_time` from wp_garden_unit where id = #{id}")
    GardenUnit findGardenUnitById(int id);

    @Update("<script>" +
            "update wp_garden_unit " +
            "<set>" +
            "<if test=\"gardenId != null\">`garden_id` = #{gardenId}, </if>" +
            "<if test=\"gardenUnitNum != null\">`garden_unit_num` = #{gardenUnitNum}, </if>" +
            "<if test=\"describe != null\">`describe` = #{describe}, </if>" +
            "<if test=\"path != null\">`path` = #{path}, </if>" +
            "<if test=\"floorPlan != null\">`floor_plan` = #{floorPlan}, </if>" +
            "<if test=\"area != null\">`area` = #{area}, </if>" +
            "<if test=\"bitmapPath != null\">`bitmap_path` = #{bitmapPath}, </if>" +
            "<if test=\"sort != null\">`sort` = #{sort}, </if>" +
            "<if test=\"status != null\">`status` = #{status}, </if>" +
            "<if test=\"createTime != null\">`create_time` = #{createTime} </if>" +
            "</set>" +
            " where id = #{id}" +
            "</script>")
    int updateGardenUnit(GardenUnit gardenUnit);

    @Delete(" delete from wp_garden_unit where id = #{id} ")
    int remove(int id);
}
