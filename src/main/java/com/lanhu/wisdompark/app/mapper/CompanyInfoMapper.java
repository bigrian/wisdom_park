package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.CompanyInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 入驻企业相关接口mapper
 * @author whongyu
 * @create by 2019/5/21
 */
@Repository
public interface CompanyInfoMapper {

    @Select("<script>" +
            "select `id`,`company_name`,`describe`,`garden_id`,`place`,`logo`,`type`,`scale`,`license`,`formats`,`is_league`,`link_man_name`,`mobile`,`spare_field`,`status`,`create_time`,`update_time` from wp_company_info " +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"companyName != null and companyName != ''\"> and company_name = #{companyName} </if>" +
            "<if test=\"describe != null and describe != ''\"> and describe = #{describe} </if>" +
            "<if test=\"gardenId != null and gardenId != ''\"> and garden_id = #{gardenId} </if>" +
            "<if test=\"place != null and place != ''\"> and place = #{place} </if>" +
            "<if test=\"logo != null and logo != ''\"> and logo = #{logo} </if>" +
            "<if test=\"type != null and type != ''\"> and type = #{type} </if>" +
            "<if test=\"scale != null and scale != ''\"> and scale = #{scale} </if>" +
            "<if test=\"license != null and license != ''\"> and license = #{license} </if>" +
            "<if test=\"formats != null and formats != ''\"> and formats = #{formats} </if>" +
            "<if test=\"isLeague != null and isLeague != ''\"> and is_league = #{isLeague} </if>" +
            "<if test=\"linkManName != null and linkManName != ''\"> and link_man_name = #{linkManName} </if>" +
            "<if test=\"mobile != null and mobile != ''\"> and mobile = #{mobile} </if>" +
            "<if test=\"spareField != null and spareField != ''\"> and spare_field = #{spareField} </if>" +
            "<if test=\"status != null and status != ''\"> and status = #{status} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "<if test=\"updateTime != null and updateTime != ''\"> and update_time = #{updateTime} </if>" +
            "</where>" +
            "<choose>" +
    "             <when test=\"sort != null and sort.trim() != ''\">" +
    "                order by ${sort} ${order}" +
    "             </when>" +
            "   <otherwise>" +
            "        order by id desc" +
            "   </otherwise>" +
            "</choose>" +
            "</script>")
    @ResultType(CompanyInfo.class)
    List<CompanyInfo> findAll(Map<String, Object> map);

    @Select("select `id`,`company_name`,`describe`,`garden_id`,`place`,`logo`,`type`,`scale`,`license`,`formats`,`is_league`,`link_man_name`,`mobile`,`spare_field`,`status`,`create_time`,`update_time` from wp_company_info where garden_id = #{gardenId} ")
    List<CompanyInfo> findAllCompanyByGardenId(String gardenId);

    @Select("select `id`,`company_name`,`describe`,`garden_id`,`place`,`logo`,`type`,`scale`,`license`,`formats`,`is_league`,`link_man_name`,`mobile`,`spare_field`,`status`,`create_time`,`update_time` from wp_company_info where id = #{companyId} ")
    CompanyInfo findCompanyByCompanyId(String companyId);

    @Select("select count(*) as countCompany from wp_company_info where garden_id = #{gardenId}")
    int countCompany(String gardentId);

    @Update("<script>" +
            "update wp_company_info " +
            "<set>" +
            "<if test=\"companyName != null\">`company_name` = #{companyName}, </if>" +
            "<if test=\"describe != null\">`describe` = #{describe}, </if>" +
            "<if test=\"gardenId != null\">`garden_id` = #{gardenId}, </if>" +
            "<if test=\"place != null\">`place` = #{place}, </if>" +
            "<if test=\"logo != null\">`logo` = #{logo}, </if>" +
            "<if test=\"type != null\">`type` = #{type}, </if>" +
            "<if test=\"scale != null\">`scale` = #{scale}, </if>" +
            "<if test=\"license != null\">`license` = #{license}, </if>" +
            "<if test=\"formats != null\">`formats` = #{formats}, </if>" +
            "<if test=\"isLeague != null\">`is_league` = #{isLeague}, </if>" +
            "<if test=\"linkManName != null\">`link_man_name` = #{linkManName}, </if>" +
            "<if test=\"mobile != null\">`mobile` = #{mobile}, </if>" +
            "<if test=\"spareField != null\">`spare_field` = #{spareField}, </if>" +
            "<if test=\"status != null\">`status` = #{status} </if>" +
            "</set>" +
            "where id = #{id}" +
            "</script>")
    int updateCompanyInfo(CompanyInfo companyInfo);

    @Insert("insert into wp_company_info(`id`, `company_name`,`describe`, `garden_id`, `place`,`logo`,`type`,`scale`,`license`,`formats`,`is_league`,`link_man_name`, `mobile`,`spare_field`,`status`) values(" +
            "#{id},#{companyName},#{describe},#{gardenId},#{place},#{logo},#{type},#{scale},#{license},#{formats},#{isLeague},#{linkManName},#{mobile},#{spareField},#{status})")
    int add(CompanyInfo companyInfo);
}
