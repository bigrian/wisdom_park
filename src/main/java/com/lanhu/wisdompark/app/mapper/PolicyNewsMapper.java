package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.PolicyNews;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/21
 */
@Repository
public interface PolicyNewsMapper {

    @Select("select `id`,`title`,`content`,`place`,`type`,`garden_id`,`head_img_path`,`status`,`create_time`,`update_time` from wp_policy_news where garden_id = #{gardenId} order by create_time asc limit 1")
    PolicyNews getNewest(String gardenId);

    @Select("<script>" +
            "select `id`,`title`,`content`,`place`,`type`,`garden_id`,`head_img_path`,`status`,`create_time`,`update_time` from wp_policy_news " +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"title != null and title != ''\"> and title = #{title} </if>" +
            "<if test=\"content != null and content != ''\"> and content = #{content} </if>" +
            "<if test=\"place != null and place != ''\"> and place = #{place} </if>" +
            "<if test=\"type != null and type != ''\"> and type = #{type} </if>" +
            "<if test=\"gardenId != null and gardenId != ''\"> and garden_id = #{gardenId} </if>" +
            "<if test=\"headImgPath != null and headImgPath != ''\"> and head_img_path = #{headImgPath} </if>" +
            "<if test=\"status != null and status != ''\"> and status = #{status} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "<if test=\"updateTime != null and updateTime != ''\"> and update_time = #{updateTime} </if>" +
            "</where>" +
            "<choose>" +
            "   <when test=\"sort != null and sort.trim() != ''\">" +
            "         order by ${sort} ${order}" +
            "    </when>" +
            "<otherwise>" +
            "         order by id desc" +
            "</otherwise>" +
            "</choose>" +
            "</script>")
    @ResultType(PolicyNews.class)
    List<PolicyNews> find(Map<String, Object> map);

    @Select(" select `id`,`title`,`content`,`place`,`type`,`garden_id`,`head_img_path`,`status`,`create_time`,`update_time` from wp_policy_news where id = #{id} ")
    PolicyNews getPolicyNewsById(int id);

    @Insert(" insert into wp_policy_news(`title`, `content`, `place`, `type`, `garden_id`, `head_img_path`, `status`) values(#{title}, #{content}, #{place}, #{type}, #{gardenId}, #{headImgPath}, #{status})")
    int add(PolicyNews policyNews);

    @Update("<script>" +
            "update wp_policy_news " +
            "<set>" +
            "<if test=\"title != null\">`title` = #{title}, </if>" +
            "<if test=\"content != null\">`content` = #{content}, </if>" +
            "<if test=\"place != null\">`place` = #{place}, </if>" +
            "<if test=\"type != null\">`type` = #{type}, </if>" +
            "<if test=\"gardenId != null\">`garden_id` = #{gardenId}, </if>" +
            "<if test=\"headImgPath != null\">`head_img_path` = #{headImgPath}, </if>" +
            "<if test=\"status != null\">`status` = #{status} </if>" +
            "</set>" +
            "where id = #{id}" +
            "</script>")
    int update(PolicyNews policyNews);
}
