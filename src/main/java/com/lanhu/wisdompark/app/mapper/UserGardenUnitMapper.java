package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.UserGardenUnit;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/25
 */
@Repository
public interface UserGardenUnitMapper {

    @Select("<script>" +
            "select count(*) as countAppoint from wp_user_garden_unit " +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"wxAppUserId != null and wxAppUserId != ''\"> and wx_app_user_id = #{wxAppUserId} </if>" +
            "<if test=\"gardenUnitId != null and gardenUnitId != ''\"> and garden_unit_id = #{gardenUnitId} </if>" +
            "<if test=\"gardenId != null and gardenId != ''\"> and garden_id = #{gardenId} </if>" +
            "<if test=\"type != null and type != ''\"> and type = #{type} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "</where>" +
            "</script>")
    @ResultType(int.class)
    int countAppoint(Map<String, Object> map);

    @Insert("insert into wp_user_garden_unit(`wx_app_user_id`, `garden_unit_id`, `garden_id`,`type`) values(#{wxAppUserId}, #{gardenUnitId}, #{gardenId}, #{type})")
    int add(UserGardenUnit userGardenUnit);

    @Delete("delete from wp_user_garden_unit where garden_id = #{gardenId} and garden_unit_id =#{gardenUnitId} and wx_app_user_id = #{userId} and type =#{type}")
    int remove(String gardenId,int gardenUnitId,int userId,int type);

    @Select("<script>" +
            "select `id`,`wx_app_user_id`,`garden_unit_id`,`garden_id`,`type`,`create_time` from wp_user_garden_unit " +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"wxAppUserId != null and wxAppUserId != ''\"> and wx_app_user_id = #{wxAppUserId} </if>" +
            "<if test=\"gardenUnitId != null and gardenUnitId != ''\"> and garden_unit_id = #{gardenUnitId} </if>" +
            "<if test=\"gardenId != null and gardenId != ''\"> and garden_id = #{gardenId} </if>" +
            "<if test=\"type != null and type != ''\"> and type = #{type} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "</where>" +
            "  <choose>" +
            "      <when test=\"sort != null and sort.trim() != ''\">" +
            "         order by ${sort} ${order}" +
            "      </when>" +
            "<otherwise>" +
            "         order by create_time desc" +
            "</otherwise>" +
            "</choose>" +
            "<if test=\" limit != null \">" +
            " limit #{limit}" +
            "</if>" +
            "</script>")
    List<UserGardenUnit> findAll(Map<String, Object> map);

    @Select(" select a.`garden_id`,a.`create_time`,b.head_path from wp_user_garden_unit a left join wp_wx_app_user b on b.id=a.wx_app_user_id where a.garden_id=#{gardenId} group by a.wx_app_user_id order by a.create_time desc limit 8 ")
    List<UserGardenUnit> findVisitUnit(String gardenId);
}
