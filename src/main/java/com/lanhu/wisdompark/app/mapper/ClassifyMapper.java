package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.Classify;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/6/22
 */
@Repository
public interface ClassifyMapper {

    @Insert(" insert into wp_classify(`name`, `level`, `parent_id`, `type`, `sort`, `remark`, `status`, `create_time`) values(#{name}, #{level}, #{parentId}, #{type}, #{sort}, #{remark}, #{status}, #{createTime}) ")
    int add(Classify classify);

    @Select(" select `id`,`name`,`level`,`parent_id`,`type`,`sort`,`remark`,`status`,`create_time` from wp_classify where id = #{id} ")
    Classify get(int id);

    @Select("<script>" +
            "select `id`,`name`,`level`,`parent_id`,`type`,`sort`,`remark`,`status`,`create_time` from wp_classify " +
            " <where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"name != null and name != ''\"> and name = #{name} </if>" +
            "<if test=\"level != null and level != ''\"> and level = #{level} </if>" +
            "<if test=\"parentId != null and parentId != ''\"> and parent_id = #{parentId} </if>" +
            "<if test=\"type != null and type != ''\"> and type = #{type} </if>" +
            "<if test=\"remark != null and remark != ''\"> and remark = #{remark} </if>" +
            "<if test=\"status != null and status != ''\"> and status = #{status} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "</where>" +
            "<choose>" +
            "   <when test=\"sort != null and sort.trim() != ''\">" +
            "      order by ${sort} ${order}" +
            "   </when>" +
            "<otherwise>" +
            "      order by id asc" +
            "</otherwise>" +
            "</choose>" +
            "</script>")
    @ResultType(Classify.class)
    List<Classify> find(Map<String,Object> map);

    @Update("<script>" +
            "update wp_classify " +
            "<set>" +
            "<if test=\"name != null\">`name` = #{name}, </if>" +
            "<if test=\"level != null\">`level` = #{level}, </if>" +
            "<if test=\"parentId != null\">`parent_id` = #{parentId}, </if>" +
            "<if test=\"type != null\">`type` = #{type}, </if>" +
            "<if test=\"sort != null\">`sort` = #{sort}, </if>" +
            "<if test=\"remark != null\">`remark` = #{remark}, </if>" +
            "<if test=\"status != null\">`status` = #{status}, </if>" +
            "<if test=\"createTime != null\">`create_time` = #{createTime}</if>" +
            "</set>" +
            "where id = #{id}" +
            "</script>")
    int update(Classify classify);

    @Update("delete from wp_classify where id = #{id}")
    int remove(int id);
}
