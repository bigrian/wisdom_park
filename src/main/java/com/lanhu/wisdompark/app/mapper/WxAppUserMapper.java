package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.WxAppUser;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/21
 */
@Repository
public interface WxAppUserMapper {

    @Select("select a.*,b.create_time as insertTime from wp_wx_app_user a left join wp_visit_garden b on a.id=b.wx_app_user_id where garden_id=#{gardenId} group by a.id order by b.create_time desc limit 8 ")
    List<WxAppUser>  getVisitInfo(String gardenId);

    @Select("<script>" +
            "select `id`,`mobile`,`user_name`,`passwork`,`nick_name`,`head_path`,`province`,`city`,`county`,`address`,`open_id`,`session_key`,`unionid`,`country`,`sex`,`status`,`type`,`create_time`,`update_time` from wp_wx_app_user " +
            " <where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"mobile != null and mobile != ''\"> and mobile = #{mobile} </if>" +
            "<if test=\"userName != null and userName != ''\"> and user_name = #{userName} </if>" +
            "<if test=\"passwork != null and passwork != ''\"> and passwork = #{passwork} </if>" +
            "<if test=\"nickName != null and nickName != ''\"> and nick_name = #{nickName} </if>" +
            "<if test=\"headPath != null and headPath != ''\"> and head_path = #{headPath} </if>" +
            "<if test=\"province != null and province != ''\"> and province = #{province} </if>" +
            "<if test=\"city != null and city != ''\"> and city = #{city} </if>" +
            "<if test=\"county != null and county != ''\"> and county = #{county} </if>" +
            "<if test=\"address != null and address != ''\"> and address = #{address} </if>" +
            "<if test=\"openId != null and openId != ''\"> and open_id = #{openId} </if>" +
            "<if test=\"sessionKey != null and sessionKey != ''\"> and session_key = #{sessionKey} </if>" +
            "<if test=\"unionid != null and unionid != ''\"> and unionid = #{unionid} </if>" +
            "<if test=\"country != null and country != ''\"> and country = #{country} </if>" +
            "<if test=\"sex != null and sex != ''\"> and sex = #{sex} </if>" +
            "<if test=\"status != null and status != ''\"> and status = #{status} </if>" +
            "<if test=\"type != null and type != ''\"> and type = #{type} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "<if test=\"updateTime != null and updateTime != ''\"> and update_time = #{updateTime} </if>" +
            "</where>" +
            "order by id asc" +
            "</script>")
    @ResultType(WxAppUser.class)
    List<WxAppUser>  findAllUser(Map<String,Object> map);

    @Select("select `id`,`mobile`,`user_name`,`passwork`,`nick_name`,`head_path`,`province`,`city`,`county`,`address`,`open_id`,`session_key`,`unionid`,`country`,`sex`,`status`,`type`,`create_time`,`update_time` from wp_wx_app_user where open_id = #{openId}")
    WxAppUser findByOpenId(String openId);

    @Select("select `id`,`mobile`,`user_name`,`passwork`,`nick_name`,`head_path`,`province`,`city`,`county`,`address`,`open_id`,`session_key`,`unionid`,`country`,`sex`,`status`,`type`,`create_time`,`update_time` from wp_wx_app_user where id = #{id}")
    WxAppUser findById(int id);

    @Update("<script>" +
            "update wp_wx_app_user " +
            "<set>" +
            "<if test=\"mobile != null\">`mobile` = #{mobile}, </if>" +
            "<if test=\"userName != null\">`user_name` = #{userName}, </if>" +
            "<if test=\"passwork != null\">`passwork` = #{passwork}, </if>" +
            "<if test=\"nickName != null\">`nick_name` = #{nickName}, </if>" +
            "<if test=\"headPath != null\">`head_path` = #{headPath}, </if>" +
            "<if test=\"province != null\">`province` = #{province}, </if>" +
            "<if test=\"city != null\">`city` = #{city}, </if>" +
            "<if test=\"county != null\">`county` = #{county}, </if>" +
            "<if test=\"address != null\">`address` = #{address}, </if>" +
            "<if test=\"openId != null\">`open_id` = #{openId}, </if>" +
            "<if test=\"sessionKey != null\">`session_key` = #{sessionKey}, </if>" +
            "<if test=\"unionid != null\">`unionid` = #{unionid}, </if>" +
            "<if test=\"country != null\">`country` = #{country}, </if>" +
            "<if test=\"sex != null\">`sex` = #{sex}, </if>" +
            "<if test=\"status != null\">`status` = #{status}, </if>" +
            "<if test=\"type != null\">`type` = #{type}, </if>" +
            "<if test=\"createTime != null\">`create_time` = #{createTime}, </if>" +
            "<if test=\"updateTime != null\">`update_time` = #{updateTime} </if>" +
            "</set>" +
            " where id = #{id}" +
            "</script>")
    int update(WxAppUser wxAppUser);

    @Insert("insert into wp_wx_app_user(`mobile`, `user_name`,`passwork`,`nick_name`, `head_path`, `province`,`city`, `county`, `address`, `open_id`,`session_key`, `unionid`, `country`, `sex`,`status`,`type`) values" +
            "(#{mobile}, #{userName}, #{passwork}, #{nickName}, #{headPath}, #{province}, #{city}, #{county}, #{address}, #{openId}, #{sessionKey}, #{unionid}, #{country}, #{sex}, #{status}, #{type})")
    @Options(keyProperty = "id",useGeneratedKeys = true)
    int add(WxAppUser wxAppUser);
}
