package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.GardenInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/21
 */
@Repository
public interface GardenInfoMapper {

    @Select("<script>" +
            " select `id`,`name`,`short_name`,`province`,`city`,`county`,`address`,`floor_plan`,`area`,`describe`,`open_date`,`head_img_path`,`pdf_path`,`longitude`,`latitude`,`sort`,`status`,`is_recommend`,`app_id`,`secret`,`create_time`,`update_time` from wp_garden_info " +
            "<choose>" +
            "   <when test=\"sort != null and sort.trim() != ''\">" +
            "       order by ${sort} ${order}" +
            "   </when>" +
            "   <otherwise>" +
            "       order by id desc" +
            "   </otherwise>" +
            "</choose>" +
            "</script>")
    List<GardenInfo> findAll(Map<String,Object> map);

    @Select(" select `id`,`name`,`short_name`,`province`,`city`,`county`,`address`,`floor_plan`,`area`,`describe`,`open_date`,`head_img_path`,`pdf_path`,`longitude`,`latitude`,`sort`,`status`,`is_recommend`,`app_id`,`secret`,`create_time`,`update_time` from wp_garden_info where id = #{id}")
    GardenInfo findGardenById(String id);

    @Insert("insert into wp_garden_info(`id`, `name`, `short_name`, `province`, `city`, `county`, `address`, `floor_plan`, `area`, `describe`, `open_date`,`head_img_path`,`pdf_path`, `longitude`, `latitude`,`sort`,`status`,`is_recommend`, `app_id`,`secret`) " +
            "values(#{id}, #{name}, #{shortName}, #{province}, #{city}, #{county}, #{address}, #{floorPlan}, #{area}, #{describe}, #{openDate},  #{headImgPath}, #{pdfPath},#{longitude}, #{latitude},#{sort},#{status},#{isRecommend},#{appId},#{secret})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    int addGardenInfo(GardenInfo gardenInfo);

    @Update("<script>" +
            "update wp_garden_info " +
            "<set>" +
            "<if test=\"name != null\">`name` = #{name}, </if>" +
            "<if test=\"shortName != null\">`short_name` = #{shortName}, </if>" +
            "<if test=\"province != null\">`province` = #{province}, </if>" +
            "<if test=\"city != null\">`city` = #{city}, </if>" +
            "<if test=\"county != null\">`county` = #{county}, </if>" +
            "<if test=\"address != null\">`address` = #{address}, </if>" +
            "<if test=\"floorPlan != null\">`floor_plan` = #{floorPlan}, </if>" +
            "<if test=\"area != null\">`area` = #{area}, </if>" +
            "<if test=\"describe != null\">`describe` = #{describe}, </if>" +
            "<if test=\"openDate != null\">`open_date` = #{openDate}, </if>" +
            "<if test=\"headImgPath != null\">`head_img_path` = #{headImgPath}, </if>" +
            "<if test=\"pdfPath != null\">`pdf_path` = #{pdfPath}, </if>" +
            "<if test=\"longitude != null\">`longitude` = #{longitude}, </if>" +
            "<if test=\"latitude != null\">`latitude` = #{latitude}, </if>" +
            "<if test=\"sort != null\">`sort` = #{sort}, </if>" +
            "<if test=\"status != null\">`status` = #{status}, </if>" +
            "<if test=\"isRecommend != null\">`is_recommend` = #{isRecommend}, </if>" +
            "<if test=\"appId != null\">`app_id` = #{appId}, </if>" +
            "<if test=\"secret != null\">`secret` = #{secret}, </if>" +
            "<if test=\"createTime != null\">`create_time` = #{createTime}, </if>" +
            "<if test=\"updateTime != null\">`update_time` = #{updateTime} </if>" +
            "</set>" +
            "where id = #{id}" +
            "</script>")
    int UpdateGardenInfo(GardenInfo gardenInfo);

    @Select(" select secret from wp_garden_info where app_id=#{appId} ")
    String findSecert(String appId);

    @Select("select b.* from wp_user_garden a left join wp_garden_info b on a.garden_id=b.id where a.user_id=#{userId}")
    List<GardenInfo> findGardenByUser(int userId);

    @Select(" select a.* from wp_garden_info a left join wp_user_garden b on b.garden_id=a.id where b.user_id =(select distinct(user_id) from wp_user_garden where garden_id=#{gardenId}) ")
    List<GardenInfo> findAllUserDown(String gardenId);
}
