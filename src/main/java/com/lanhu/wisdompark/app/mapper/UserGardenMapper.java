package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.UserGarden;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Repository;

/**
 * @author whongyu
 * @create by 2019/5/25
 */
@Repository
public interface UserGardenMapper {

    @Insert(" insert into wp_user_garden(user_id,garden_id) values(#{userId},#{gardenId}) ")
    int add(UserGarden userGarden);
}
