package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.Area;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 用户实体类Area对应的mybatis 的操作数据库的mapper
 * @author lfxa
 * Spring 整合 mybatis 时，一个 POJO 对应 MyBatis一个操作数据库的xml文件，每个xml文件又指向一个 mapper 接口
 * Spring Boot 则省略了 xml 文件这一环节，直接将以前xml文件中的sql写在了本类接口上
 * 以前在xml文件中写sql，现在使用注解直接写在接口上，所有sql注解都在：org.apache.ibatis.annotations 包中
 *
 */
@Repository
public interface AreaMapper {

    /**
     * 根据用户 parentCode 查询
     *
     * @param parentCode
     * @return :返回查询结果,不存在时返回 null
     * @Select ：等价于以前 xml 形式时的 <select 标签,sql写法仍然和以前一样
     */
    @Select(value = {"select * from wp_area where parentCode=#{parentCode}"})
    public List<Area> findAreaByParentCode(String parentCode);

    @Select(value = {"select * from wp_area where parentCode is null "})
    public List<Area> findAreaByNonParentCode();

    @Select(value = {"select code, name from wp_area "})
    public List<Area> findAll();

    @Select("<script>" +
            "SELECT * FROM `wp_area` " +
            "<where> " +
            "<if test=\"parentCode != null and parentCode !=''\"> and parentCode = #{parentCode} </if> " +
            "<if test=\"parentCode == null or parentCode ==''\"> and parentCode is null  </if> " +
            "<if test=\"code != null and code !=''\"> and code like CONCAT('%',#{code},'%') </if> " +
            "<if test=\"name != null and name !=''\"> and name like CONCAT('%',#{name},'%') </if> " +
            "</where> " +
            "</script>")
    public List<Area> queryArea(String code, String name, String parentCode);

    @Select(value = {" select GROUP_CONCAT(name SEPARATOR #{septor}) from wp_area where code = #{province} or code = #{city} or code = #{county} "})
    public String findNameStrByCode(String septor, String province, String city, String county);


    /**
     * 只查询area name
     * @param code
     * @return
     */
    @Select(value = {"select name from wp_area where code=#{code}"})
    public String findNameByCode(String code);

    @Select(value = {"select * from wp_area where code=#{code}"})
    public Area findAreaByCode(String code);
}
