package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/23
 */
@Repository
public interface SysUserMapper {

    @Select("<script>" +
            "select `id`,`user_name`,`password`,`mobile`,`province`,`city`,`county`,`address`,`type`,status,`create_time`,`update_time` from wp_sys_user " +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"userName != null and userName != ''\"> and user_name = #{userName} </if>" +
            "<if test=\"password != null and password != ''\"> and password = #{password} </if>" +
            "<if test=\"mobile != null and mobile != ''\"> and mobile = #{mobile} </if>" +
            "<if test=\"province != null and province != ''\"> and province = #{province} </if>" +
            "<if test=\"city != null and city != ''\"> and city = #{city} </if>" +
            "<if test=\"county != null and county != ''\"> and county = #{county} </if>" +
            "<if test=\"address != null and address != ''\"> and address = #{address} </if>" +
            "<if test=\"type != null and type != ''\"> and type = #{type} </if>" +
            "<if test=\"status != null and status != ''\"> and status = #{status} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "<if test=\"updateTime != null and updateTime != ''\"> and update_time = #{updateTime} </if>" +
            "</where>" +
            "</script>")
    @ResultType(User.class)
    List<User> findUser(Map<String, Object> map);

    @Select("select `id`,`user_name`,`password`,`mobile`,`province`,`city`,`county`,`address`,`type`,status,`create_time`,`update_time` from wp_sys_user where id = #{id}")
    User findById(int id);

    @Update("<script>" +
            "update wp_sys_user " +
            "<set>" +
            "<if test=\"userName != null\">`user_name` = #{userName}, </if>" +
            "<if test=\"password != null\">`password` = #{password}, </if>" +
            "<if test=\"mobile != null\">`mobile` = #{mobile}, </if>" +
            "<if test=\"province != null\">`province` = #{province}, </if>" +
            "<if test=\"city != null\">`city` = #{city}, </if>" +
            "<if test=\"county != null\">`county` = #{county}, </if>" +
            "<if test=\"address != null\">`address` = #{address}, </if>" +
            "<if test=\"type != null\">`type` = #{type}, </if>" +
            "<if test=\"status != null\">`status` = #{status}, </if>" +
            "<if test=\"createTime != null\">`create_time` = #{createTime}, </if>" +
            "<if test=\"updateTime != null\">`update_time` = #{updateTime} </if>" +
            "</set>" +
            "where id = #{id}" +
            "</script>")
    int update(User user);

    @Insert("insert into wp_sys_user(`user_name`, `password`, `mobile`, `province`, `city`, `county`, `address`, `type`, `status`)" +
            " values(#{userName}, #{password}, #{mobile}, #{province}, #{city}, #{county}, #{address}, #{type}, #{status})")
    int add(User user);
}
