package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.BusinessMovement;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/22
 */
@Repository
public interface BusinessMovementMapper {

    @Select("select `id`,`title`,`content`,`head_img_path`,`company_id`,`type`,`status`,`create_time`,`update_time` from wp_business_movement where company_id=#{companyId} ")
    public List<BusinessMovement> findAllMovementByCompanyId(String companyId);

    @Select("select `id`,`title`,`content`,`head_img_path`,`company_id`,`type`,`status`,`create_time`,`update_time` from wp_business_movement where id=#{id}")
    public BusinessMovement findMovementById(String id);
}
