package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.Collect;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/22
 */
@Repository
public interface CollectMapper {

    @Insert("insert into wp_collect(`user_id`, `garden_shop_id`) values(#{userId}, #{gardenShopId})")
    int addCollect(Collect collect);

    @Delete("delete from wp_collect where garden_shop_id = #{collectId} and user_id = #{userId}")
    int delectCollect(String collectId, int userId);

    @Select("select `id`,`user_id`,`garden_shop_id`,`create_time` from wp_collect where user_id = #{userId}")
    List<Collect> findUserCollect(int userId);
}
