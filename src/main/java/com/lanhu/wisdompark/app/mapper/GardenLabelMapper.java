package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.GardenLabel;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/27
 */
@Repository
public interface GardenLabelMapper {

    @Select(" select `id`,`label_name`,`garden_id`,`icon`,`sort`,`is_skip`,`status`,`create_time` from wp_garden_label where id = #{id} ")
    GardenLabel get(int id);

    @Select("<script>" +
            "select `id`,`label_name`,`garden_id`,`icon`,`sort`,`is_skip`,`status`,`create_time` from wp_garden_label" +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"labelName != null and labelName != ''\"> and label_name = #{labelName} </if>" +
            "<if test=\"gardenId != null and gardenId != ''\"> and garden_id = #{gardenId} </if>" +
            "<if test=\"isSkip != null and isSkip != ''\"> and is_skip = #{isSkip} </if>" +
            "<if test=\"status != null and status != ''\"> and status = #{status} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "</where>" +
            "   <choose>" +
            "       <when test=\"sort != null and sort.trim() != ''\">" +
            "          order by ${sort} ${order}" +
            "       </when>" +
            "   <otherwise>" +
            "          order by id desc" +
            "   </otherwise>" +
            "</choose>" +
            "</script>")
    @ResultType(GardenLabel.class)
    List<GardenLabel> findAll(Map<String,Object> map);

    @Insert(" insert into wp_garden_label(`label_name`, `garden_id`,`icon`, `sort`, `is_skip`, `status`, `create_time`) values(#{labelName}, #{gardenId},#{icon}, #{sort}, #{isSkip}, #{status}, #{createTime}) ")
    int add(GardenLabel gardenLabel);

    @Update("<script>" +
            "update wp_garden_label " +
            "<set>" +
            "<if test=\"labelName != null\">`label_name` = #{labelName}, </if>" +
            "<if test=\"gardenId != null\">`garden_id` = #{gardenId}, </if>" +
            "<if test=\"icon != null\">`icon` = #{icon}, </if>" +
            "<if test=\"sort != null\">`sort` = #{sort}, </if>" +
            "<if test=\"isSkip != null\">`is_skip` = #{isSkip}, </if>" +
            "<if test=\"status != null\">`status` = #{status}  </if>" +
            "</set>" +
            " where id = #{id}" +
            "</script>")
    int update(GardenLabel gardenLabel);

    @DeleteMapping("delete from wp_garden_label where id = #{id}")
    int remove(int id);
}
