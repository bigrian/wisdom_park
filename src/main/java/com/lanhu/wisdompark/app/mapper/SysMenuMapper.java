package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.Menu;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/6/26
 */
@Resource
public interface SysMenuMapper {

    @Insert(" insert into wp_sys_menu(`menu_name`, `icon`, `parent_id`, `action`, `module`, `status`, `user_type`) values(#{menuName}, #{icon}, #{parentId}, #{action}, #{module}, #{status}, #{userType}) ")
    int add(Menu menu);

    @Select("<script>" +
            "select count(*) from wp_sys_menu" +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"menuName != null and menuName != ''\"> and menu_name = #{menuName} </if>" +
            "<if test=\"icon != null and icon != ''\"> and icon = #{icon} </if>" +
            "<if test=\"parentId != null and parentId != ''\"> and parent_id = #{parentId} </if>" +
            "<if test=\"action != null and action != ''\"> and action = #{action} </if>" +
            "<if test=\"module != null and module != ''\"> and module = #{module} </if>" +
            "<if test=\"userType != null and userType != ''\"> and user_type like concat('%',#{userType},'%') </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "<if test=\"status != null and status != ''\"> and status = #{status} </if>" +
            "</where>" +
            "</script>")
    List<Menu> find(Map<String,Object> map);

    @Select("select `id`,`menu_name`,`icon`,`parent_id`,`action`,`module`,`status`,`user_type`,`create_time` from wp_sys_menu where id = #{id}")
    Menu get(int id);

    @Update("<script>" +
            "update wp_sys_menu " +
            "<set>" +
            "<if test=\"menuName != null\">`menu_name` = #{menuName}, </if>" +
            "<if test=\"icon != null\">`icon` = #{icon}, </if>" +
            "<if test=\"parentId != null\">`parent_id` = #{parentId}, </if>" +
            "<if test=\"action != null\">`action` = #{action}, </if>" +
            "<if test=\"module != null\">`module` = #{module}, </if>" +
            "<if test=\"userType != null\">`user_type` = #{userType}, </if>" +
            "<if test=\"createTime != null\">`create_time` = #{createTime}, </if>" +
            "<if test=\"status != null\">`status` = #{status}</if>" +
            "</set>" +
            "where id = #{id}" +
            "</script>")
    int update(Menu menu);
}
