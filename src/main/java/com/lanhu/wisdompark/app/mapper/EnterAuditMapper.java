package com.lanhu.wisdompark.app.mapper;

import com.lanhu.wisdompark.app.domain.EnterAudit;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/27
 */
@Repository
public interface EnterAuditMapper {

    @Insert("insert into wp_enter_audit(`company_name`, `formats`, `link_man_name`, `link_mobile`, `garden_id`, `unit_id`, `intention_area`,`spare`, `wx_app_user_id`, `status`, `create_time`) " +
            " values(#{companyName}, #{formats}, #{linkManName}, #{linkMobile}, #{gardenId}, #{unitId}, #{intentionArea},#{spare}, #{wxAppUserId}, #{status}, #{createTime})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    int addEnterAudit(EnterAudit enterAudit);

    @Select("<script>" +
            "select `id`,`company_name`,`formats`,`link_man_name`,`link_mobile`,`garden_id`,`unit_id`,`intention_area`,`spare`,`wx_app_user_id`,`status`,`create_time` from wp_enter_audit " +
            "<where>  " +
            "<if test=\"id != null and id != ''\"> and id = #{id} </if>" +
            "<if test=\"companyName != null and companyName != ''\"> and company_name = #{companyName} </if>" +
            "<if test=\"formats != null and formats != ''\"> and formats = #{formats} </if>" +
            "<if test=\"linkManName != null and linkManName != ''\"> and link_man_name = #{linkManName} </if>" +
            "<if test=\"linkMobile != null and linkMobile != ''\"> and link_mobile = #{linkMobile} </if>" +
            "<if test=\"gardenId != null and gardenId != ''\"> and garden_id = #{gardenId} </if>" +
            "<if test=\"unitId != null and unitId != ''\"> and unit_id = #{unitId} </if>" +
            "<if test=\"intentionArea != null and intentionArea != ''\"> and intention_area = #{intentionArea} </if>" +
            "<if test=\"spare != null and spare != ''\"> and spare = #{spare} </if>" +
            "<if test=\"wxAppUserId != null and wxAppUserId != ''\"> and wx_app_user_id = #{wxAppUserId} </if>" +
            "<if test=\"status != null and status != ''\"> and status = #{status} </if>" +
            "<if test=\"createTime != null and createTime != ''\"> and create_time = #{createTime} </if>" +
            "</where>" +
            " order by id desc" +
            "</script>")
    @ResultType(EnterAudit.class)
    List<EnterAudit> findAll(Map<String,Object> map);

    @Select(" select `id`,`company_name`,`formats`,`link_man_name`,`link_mobile`,`garden_id`,`unit_id`,`intention_area`,`spare`,`wx_app_user_id`,`status`,`create_time` from wp_enter_audit where id = #{id}")
    EnterAudit find(String id);

    @Update("<script>" +
            "update wp_enter_audit " +
            "<set>" +
            "<if test=\"companyName != null\">`company_name` = #{companyName}, </if>" +
            "<if test=\"formats != null\">`formats` = #{formats}, </if>" +
            "<if test=\"linkManName != null\">`link_man_name` = #{linkManName}, </if>" +
            "<if test=\"linkMobile != null\">`link_mobile` = #{linkMobile}, </if>" +
            "<if test=\"gardenId != null\">`garden_id` = #{gardenId}, </if>" +
            "<if test=\"unitId != null\">`unit_id` = #{unitId}, </if>" +
            "<if test=\"intentionArea != null\">`intention_area` = #{intentionArea}, </if>" +
            "<if test=\"spare != null\">`spare` = #{spare}, </if>" +
            "<if test=\"wxAppUserId != null\">`wx_app_user_id` = #{wxAppUserId}, </if>" +
            "<if test=\"status != null\">`status` = #{status} </if>" +
            "</set>" +
            "where id = #{id}" +
            "</script>")
    int update(EnterAudit enterAudit);

}
