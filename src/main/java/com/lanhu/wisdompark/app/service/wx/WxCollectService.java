package com.lanhu.wisdompark.app.service.wx;

import com.lanhu.wisdompark.app.domain.Collect;
import com.lanhu.wisdompark.app.mapper.CollectMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/22
 */
@Service
public class WxCollectService {

    @Resource
    private CollectMapper collectMapper;

    /**
     * 用户收藏园区/商铺 type 0 收藏 1取消收藏
     * @param collectId
     * @param userId
     * @param type
     * @return
     */
    public int userCollectOperate(String collectId, int userId, int type){
        int i = 0;
        Collect collect = null;
        if (0 == type){
            collect = new Collect();
            collect.setUserId(userId);
            collect.setGardenShopId(collectId);
            i = collectMapper.addCollect(collect);
        }else {
            i = collectMapper.delectCollect(collectId,userId);
        }
        return i;
    }

    public List<Collect> findUserCollect(int userId){
        return collectMapper.findUserCollect(userId);
    }

}
