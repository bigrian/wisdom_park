package com.lanhu.wisdompark.app.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.WxAppUser;
import com.lanhu.wisdompark.app.mapper.WxAppUserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/23
 */
@Service
public class WxAppUserService {

    @Resource
    private WxAppUserMapper wxAppUserMapper;

    /**
     * 查询所有 用户列表 分页
     * @return
     */
    public PageInfo<WxAppUser> findAllUser(int pageNum, int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        Map<String,Object> map = new HashMap<>();
        List<WxAppUser> allUser = wxAppUserMapper.findAllUser(map);
        PageInfo<WxAppUser> pageInfo = new PageInfo<>(allUser);
        return pageInfo;
    }

    /**
     * 用户多条件查询
     * @param province
     * @param city
     * @param county
     * @param userName
     * @param mobile
     * @return
     */
    public List<WxAppUser> findMultiple(String province,String city,String county,String userName,String mobile){
        Map<String,Object> map = new HashMap<>();
        if(!"-1".equals(province)){
            map.put("province",province);
        }
        if(!"-1".equals(city)){
            map.put("city",city);
        }
        if(!"-1".equals(county)){
            map.put("county",county);
        }
        if(!"-1".equals(userName)){
            map.put("userName",userName);
        }
        if(!"-1".equals(mobile)){
            map.put("mobile",mobile);
        }
        List<WxAppUser> allUser = wxAppUserMapper.findAllUser(map);
        return allUser;
    }

    public int updateWxAppUser(WxAppUser wxAppUser){
        return wxAppUserMapper.update(wxAppUser);
    }

    public WxAppUser findWxAppUserById(int id){
        return wxAppUserMapper.findById(id);
    }

}
