package com.lanhu.wisdompark.app.service;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.GardenInfo;
import com.lanhu.wisdompark.app.domain.UserGarden;
import com.lanhu.wisdompark.app.domain.WpResource;
import com.lanhu.wisdompark.app.mapper.GardenInfoMapper;
import com.lanhu.wisdompark.app.mapper.ResourceMapper;
import com.lanhu.wisdompark.app.mapper.UserGardenMapper;
import com.lanhu.wisdompark.app.util.UuidUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/21
 */
@Service
public class GardenInfoService {

    @Resource
    private GardenInfoMapper gardenInfoMapper;
    @Resource
    private ResourceMapper resourceMapper;
    @Resource
    private UserGardenMapper userGardenMapper;
    @Resource
    private AreaService areaService;
    @Autowired
    private OrientationService orientationService;

    /**
     * 查询该用户所有 园区列表（分页）
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageInfo<GardenInfo> findGardenByUser(int userId,int pageNum, int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<GardenInfo> companyInfos = gardenInfoMapper.findGardenByUser(userId);
        PageInfo<GardenInfo> pageInfo = new PageInfo<>(companyInfos);
        return pageInfo;
    }

    /**
     * 添加 园区信息
     * @param gardenInfo
     * @return
     */
    public int addGardenInfo(GardenInfo gardenInfo){
        List<WpResource> resources = gardenInfo.getWpResourceList();
        String uuid = UuidUtil.getUUID();
        gardenInfo.setId(uuid);
        if ( resources == null || resources.isEmpty() || "".equals(gardenInfo.getPdfPath())){
            return -1;
        }
        for (WpResource ws : resources){
            ws.setResourceId(uuid);
            if(ws.getSort() == 1){
                gardenInfo.setHeadImgPath(ws.getPath());//设置第二张图为首图
            }
        }
        if (!"".equals(gardenInfo.getWpResourcePdf())){
            gardenInfo.getWpResourcePdf().setResourceId(uuid);
            resources.add(gardenInfo.getWpResourcePdf());
            gardenInfo.setPdfPath(gardenInfo.getWpResourcePdf().getPath());
        }
        String addressName = areaService.findNameByCode(gardenInfo.getProvince())+areaService.findNameByCode(gardenInfo.getCity())+areaService.findNameByCode(gardenInfo.getCounty())+gardenInfo.getAddress();
        JSONObject longLat2 = orientationService.getLongLat2(addressName);//定位 获取经纬度
        gardenInfo.setLongitude(longLat2.getDouble("lng"));
        gardenInfo.setLatitude(longLat2.getDouble("lat"));
        int j = resourceMapper.batchUpdate(resources);
        int i = gardenInfoMapper.addGardenInfo(gardenInfo);

        UserGarden userGarden = new UserGarden();
        userGarden.setGardenId(gardenInfo.getId());
        userGarden.setUserId(gardenInfo.getUserId());
        int k = userGardenMapper.add(userGarden);

        if(i > 0 && j > 0 && k > 0){
            return 1;
        }else {
            return 0;
        }
    }

    /**
     * 编辑 园区信息
     * @param gardenInfo
     * @return
     */
    public int UpdateGardenInfo(GardenInfo gardenInfo){
        List<WpResource> resources = gardenInfo.getWpResourceList();//轮播图
        GardenInfo garden = gardenInfoMapper.findGardenById(gardenInfo.getId());
        if (null != gardenInfo.getWpResourcePdf() && !garden.getPdfPath().equals(gardenInfo.getWpResourcePdf().getPath())){
            gardenInfo.getWpResourcePdf().setResourceId(gardenInfo.getId());
            resources.add(gardenInfo.getWpResourcePdf());
            gardenInfo.setPdfPath(gardenInfo.getWpResourcePdf().getPath());
        }
        if (!garden.getAddress().equals(gardenInfo.getAddress())){
            String addressName = areaService.findNameByCode(gardenInfo.getProvince())+areaService.findNameByCode(gardenInfo.getCity())+areaService.findNameByCode(gardenInfo.getCounty())+gardenInfo.getAddress();
            JSONObject longLat2 = orientationService.getLongLat2(addressName);//定位 获取经纬度
            gardenInfo.setLongitude(longLat2.getDouble("lng"));
            gardenInfo.setLatitude(longLat2.getDouble("lat"));
        }
        if(resources.size() > 0 && resources != null){
            for (WpResource ws : resources){
               ws.setResourceId(gardenInfo.getId());
               if(ws.getSort() == 1){
                  gardenInfo.setHeadImgPath(ws.getPath());//设置第二张图为首图
               }
            }
            resourceMapper.batchUpdate(resources);
        }

        gardenInfo.setUpdateTime(new DateTime().toString("yyyy-MM-dd HH:mm:ss"));
        int i = gardenInfoMapper.UpdateGardenInfo(gardenInfo);
        if(i>0){
            return 1;
        }else {
            return 0;
        }
    }

    public GardenInfo queryGardenById(String gardenId){
        return gardenInfoMapper.findGardenById(gardenId);
    }

    public int saveGarden(GardenInfo gardenInfo){
        return gardenInfoMapper.addGardenInfo(gardenInfo);
    }

    public GardenInfo findGardenById(String id){
        GardenInfo garden = gardenInfoMapper.findGardenById(id);
        List<WpResource> resources = resourceMapper.getResource(id, 0);//轮播图
        garden.setWpResourceList(resources);
        return garden;
    }

    public String findSecret(String appId){
        return gardenInfoMapper.findSecert(appId);
    }
}
