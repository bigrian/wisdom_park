package com.lanhu.wisdompark.app.service.wx;

import com.lanhu.wisdompark.app.domain.WxAppUser;
import com.lanhu.wisdompark.app.mapper.WxAppUserMapper;
import com.lanhu.wisdompark.app.util.OrientationUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author whongyu
 * @create by 2019/5/22
 */
@Service
public class WxUserService {

    @Resource
    private WxAppUserMapper wxAppUserMapper;

    public WxAppUser findByOpenId(String openId){
        return wxAppUserMapper.findByOpenId(openId);
    }

    public WxAppUser findById(int id){
        return wxAppUserMapper.findById(id);
    }

    public int add(WxAppUser wxAppUser){
        return wxAppUserMapper.add(wxAppUser);
    }

    public int update(WxAppUser wxAppUser){
        if(wxAppUser.getLatitude() != 0.0 && wxAppUser.getLatitude() !=0.0){
            String adress = OrientationUtil.getAdress(wxAppUser.getLongitude(), wxAppUser.getLatitude());
        }
        return wxAppUserMapper.update(wxAppUser);
    }
}
