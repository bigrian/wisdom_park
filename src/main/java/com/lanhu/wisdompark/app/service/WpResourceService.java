package com.lanhu.wisdompark.app.service;

import com.lanhu.wisdompark.app.domain.WpResource;
import com.lanhu.wisdompark.app.mapper.ResourceMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/21
 */
@Service
public class WpResourceService {

    @Resource
    private ResourceMapper resourceMapper;

    /**
     * 上传保存路径
     * @param wpResource
     * @return
     */
    public int addWpResource(WpResource wpResource){
        return resourceMapper.addWpResource(wpResource);
    }

    /**
     *  删除 文件路径
     * @param id
     * @return
     */
    public int deleteResource(int id){
        return resourceMapper.deleteResource(id);
    }

    public List<WpResource> findAdvert(String gardenId){
        Map<String, Object> map = new HashMap<>();
        map.put("type",4);
        map.put("sort","sort");
        map.put("resourceId",gardenId);
        return resourceMapper.findAll(map);
    }
}
