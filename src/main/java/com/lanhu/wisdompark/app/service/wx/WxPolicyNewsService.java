package com.lanhu.wisdompark.app.service.wx;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.PolicyNews;
import com.lanhu.wisdompark.app.mapper.PolicyNewsMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/6/25
 */
@Service
public class WxPolicyNewsService {

    @Resource
    private PolicyNewsMapper policyNewsMapper;

    public PageInfo<PolicyNews> findNews(String gardenId,int pageNum,int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        Map<String,Object> map = new HashMap<>();
        map.put("gardenId",gardenId);
        List<PolicyNews> policyNews = policyNewsMapper.find(map);
        PageInfo<PolicyNews> pageInfo = new PageInfo<>(policyNews);
        return pageInfo;
    }
}
