package com.lanhu.wisdompark.app.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;


/**
 * @author whongyu
 * @create by 2019/5/22
 */


@Slf4j
@Service
public class OrientationService {

    private String  key="2KFBZ-3QXCJ-LFBF7-FJMDV-NQLCQ-OZFZ4";//腾讯地图  key

    /**
     *  腾讯-根据经纬度获取地址
     * @param longitude
     * @param latitude
     * @return
     */
    public JSONObject getAdress2(double longitude,double latitude){
        log.info("腾讯-根据经纬度获取地址方法调用成功!"+longitude+","+latitude);
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://apis.map.qq.com/ws/geocoder/v1/?location="+latitude+","+longitude+"&key="+key+"&get_poi=1";
        log.info("腾讯-根据经纬度获取地址url:"+url);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        JSONObject object = null;
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            String body = responseEntity.getBody();
            JSONObject obj = JSON.parseObject(body);
            object = obj.getJSONObject("result");
            log.info("腾讯-根据经纬度获取地址为:" + object);
        }
        return object;
    }

    /**
     * 腾讯-根据地址获取经纬度
     * @param address
     * @return
     */
    public JSONObject getLongLat2(@RequestParam String address){
        log.info("腾讯-根据地址获取经纬度调用成功!"+address);
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://apis.map.qq.com/ws/geocoder/v1/?address="+address+"&key="+key;//定位到门址
        log.info("腾讯-根据地址获取经纬度url:"+url);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        JSONObject object = null;
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            String body = responseEntity.getBody();
            JSONObject obj = JSON.parseObject(body);
            object = obj.getJSONObject("result").getJSONObject("location");
            log.info("腾讯-根据地址获取经纬度为:" + object);
        }
        return  object;
    }
}
