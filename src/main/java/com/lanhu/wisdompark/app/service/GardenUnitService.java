package com.lanhu.wisdompark.app.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.GardenUnit;
import com.lanhu.wisdompark.app.mapper.GardenUnitMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/25
 */
@Service
public class GardenUnitService {

    @Resource
    private GardenUnitMapper gardenUnitMapper;

    public int addGardenUnit(GardenUnit gardenUnit){
        return gardenUnitMapper.add(gardenUnit);
    }

    public int updateGardenUnit(GardenUnit gardenUnit){
        return gardenUnitMapper.updateGardenUnit(gardenUnit);
    }

    public GardenUnit findGardenUnitById(int id){
        return gardenUnitMapper.findGardenUnitById(id);
    }

    public int addGardenById(GardenUnit gardenUnit){
        if(0 == gardenUnit.getId()){
            return -1;
        }
        return gardenUnitMapper.updateGardenUnit(gardenUnit);
    }

    public PageInfo<GardenUnit> findGardenByGardenId(String gardenId,int pageNum,int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        Map<String,Object> map = new HashMap<>();
        map.put("gardenId",gardenId);
        List<GardenUnit> all = gardenUnitMapper.findAll(map);
        PageInfo<GardenUnit> pageInfo = new PageInfo<>(all);
        return pageInfo;
    }

    public GardenUnit findGardenById(int id){
        return gardenUnitMapper.findGardenUnitById(id);
    }

    public int removeGardenById(int id){
        return gardenUnitMapper.remove(id);
    }

    public PageInfo<GardenUnit> searchGarden(GardenUnit gardenUnit){
        PageHelper.startPage(gardenUnit.getPageNum(),gardenUnit.getPageSize());
        List<GardenUnit> gardenUnits = gardenUnitMapper.searchGarden(gardenUnit);
        PageInfo<GardenUnit> pageInfo = new PageInfo<>(gardenUnits);
        return pageInfo;
    }
}
