package com.lanhu.wisdompark.app.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.CompanyInfo;
import com.lanhu.wisdompark.app.mapper.CompanyInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/6/3
 */
@Service
public class CompanyInfoService {

    @Resource
    private CompanyInfoMapper companyInfoMapper;

    public PageInfo<CompanyInfo> findCompanyInfo(String gardenId, int pageNum, int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        Map<String,Object> map = new HashMap<>();
        map.put("gardenId",gardenId);
        List<CompanyInfo> all = companyInfoMapper.findAll(map);
        PageInfo<CompanyInfo> pageInfo = new PageInfo<>(all);
        return pageInfo;
    }

    public CompanyInfo queryCompanyInfo(String id){
        return companyInfoMapper.findCompanyByCompanyId(id);
    }

    public int updateCompanyInfo(CompanyInfo companyInfo){
        return companyInfoMapper.updateCompanyInfo(companyInfo);
    }

    public int addCompanyInfo(CompanyInfo companyInfo){
        return companyInfoMapper.add(companyInfo);
    }
}
