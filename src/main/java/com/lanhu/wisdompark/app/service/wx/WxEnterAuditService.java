package com.lanhu.wisdompark.app.service.wx;

import com.lanhu.wisdompark.app.domain.EnterAudit;
import com.lanhu.wisdompark.app.domain.UserGardenUnit;
import com.lanhu.wisdompark.app.mapper.EnterAuditMapper;
import com.lanhu.wisdompark.app.mapper.UserGardenUnitMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/27
 */
@Service
public class WxEnterAuditService {

    @Resource
    private UserGardenUnitMapper userGardenUnitMapper;
    @Resource
    private EnterAuditMapper enterAuditMapper;

    public int enterGarden(EnterAudit enterAudit){
        Map<String,Object> map = new HashMap<>();
        map.put("wxAppUserId",enterAudit.getWxAppUserId());
        map.put("unitId",enterAudit.getUnitId());
        List<EnterAudit> enterAudits = enterAuditMapper.findAll(map);
        if (enterAudits != null && enterAudits.size() > 0){
            if(enterAudits.get(0).getStatus() ==1){
                return -1;
            }
        }

        UserGardenUnit userGardenUnit = new UserGardenUnit();
        userGardenUnit.setGardenId(enterAudit.getGardenId());
        userGardenUnit.setGardenUnitId(enterAudit.getUnitId());
        userGardenUnit.setWxAppUserId(enterAudit.getWxAppUserId());
        userGardenUnit.setType(1);
        userGardenUnitMapper.add(userGardenUnit);

        return enterAuditMapper.addEnterAudit(enterAudit);
    }

    public List<EnterAudit> queryEnter(int userId){
        Map<String,Object> map = new HashMap<>();
        map.put("wxAppUserId",userId);
        return enterAuditMapper.findAll(map);
    }
}
