package com.lanhu.wisdompark.app.service.wx;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.CompanyInfo;
import com.lanhu.wisdompark.app.mapper.CompanyInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/21
 */
@Service
public class WxCompanyInfoService {

    @Resource
    private CompanyInfoMapper companyInfoMapper;

    public PageInfo<CompanyInfo> findAllCompanyByGardenId(String gardenId,int pageNum,int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<CompanyInfo> companys = companyInfoMapper.findAllCompanyByGardenId(gardenId);
        PageInfo<CompanyInfo> pageInfo = new PageInfo<>(companys);
        return pageInfo;
    }

    public CompanyInfo findCompanyByCompanyId(String companyId){
        return companyInfoMapper.findCompanyByCompanyId(companyId);
    }
}
