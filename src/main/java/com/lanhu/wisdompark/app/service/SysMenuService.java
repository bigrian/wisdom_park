package com.lanhu.wisdompark.app.service;

import com.lanhu.wisdompark.app.domain.Menu;
import com.lanhu.wisdompark.app.mapper.SysMenuMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/6/26
 */
@Service
public class SysMenuService {

    @Resource
    private SysMenuMapper sysMenuMapper;

    public int createMenu(Menu menu){
        return sysMenuMapper.add(menu);
    }

    public int editMenu(Menu menu){
        return sysMenuMapper.update(menu);
    }

    public Menu queryMenu(int id){
        return sysMenuMapper.get(id);
    }

    public List<Menu> findMenuByUserType(String userType){
        Map<String,Object> map = new HashMap<>();
        map.put("parentId",1);
        map.put("userType",userType);
        List<Menu> menus1 = sysMenuMapper.find(map);//一级菜单
        for(Menu m : menus1){
            map.clear();
            map.put("parentId",m.getId());
            map.put("userType",userType);
            List<Menu> menus2 = sysMenuMapper.find(map);//二级菜单
            m.setMenuList(menus2);
        }
        return menus1;
    }
}
