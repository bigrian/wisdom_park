package com.lanhu.wisdompark.app.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.GardenLabel;
import com.lanhu.wisdompark.app.domain.WpResource;
import com.lanhu.wisdompark.app.mapper.GardenLabelMapper;
import com.lanhu.wisdompark.app.mapper.ResourceMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/6/21
 */
@Service
public class GardenLabelService {

    @Resource
    private GardenLabelMapper gardenLabelMapper;
    @Resource
    private ResourceMapper resourceMapper;

    public PageInfo<WpResource> findAllGardenLabel(int pageNum, int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        Map<String,Object> map = new HashMap<>();
        map.put("type",5);
        List<WpResource> all = resourceMapper.findAll(map);//类型为 5 的标签图
        PageInfo<WpResource> pageInfos = new PageInfo<>(all);
        return pageInfos;
    }

    public List<GardenLabel> searchGardenLabel(String gardenId){
        Map<String,Object> map = new HashMap<>();
        map.put("gardenId",gardenId);
        return gardenLabelMapper.findAll(map);
    }

    public GardenLabel findGardenLabel(int id){
        return gardenLabelMapper.get(id);
    }

    public int addGardenLabel(GardenLabel gardenLabel){
        return gardenLabelMapper.add(gardenLabel);
    }

    public int editGardenLabel(GardenLabel gardenLabel){
        return gardenLabelMapper.update(gardenLabel);
    }

    public int removeGardenLabel(int id){
        return gardenLabelMapper.remove(id);
    }
}
