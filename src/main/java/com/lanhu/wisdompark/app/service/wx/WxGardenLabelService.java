package com.lanhu.wisdompark.app.service.wx;

import com.lanhu.wisdompark.app.domain.GardenLabel;
import com.lanhu.wisdompark.app.mapper.GardenLabelMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/27
 */
@Service
public class WxGardenLabelService {

    @Resource
    private GardenLabelMapper gardenLabelMapper;

    public List<GardenLabel> findAllLabel(String gardenId){

        Map<String,Object> map = new HashMap<>();
        map.put("gardenId",gardenId);
        map.put("sort","sort");
        map.put("order","asc");
       return gardenLabelMapper.findAll(map);
    }
}
