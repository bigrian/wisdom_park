package com.lanhu.wisdompark.app.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.Area;
import com.lanhu.wisdompark.app.mapper.AreaMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AreaService {

	@Resource
	private AreaMapper areaMapper;
	/*@Resource
	private RedisUtil redisUtil;*/

	public List<Area> findAreaByParent(String parentCode) {
		if (null == parentCode || "".equals(parentCode)) {
			return areaMapper.findAreaByNonParentCode();
		} else {
			return areaMapper.findAreaByParentCode(parentCode);
		}
	}

	public PageInfo<Area> findAreaByParent(String parentCode, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);

		List<Area> areaList = null;
		if (null == parentCode || "".equals(parentCode)) {
			areaList = areaMapper.findAreaByNonParentCode();
		} else {
			areaList = areaMapper.findAreaByParentCode(parentCode);
		}
		PageInfo<Area> pageInfo = new PageInfo<>(areaList);
		return pageInfo;
	}

	public List<Area> findAll() {
		return areaMapper.findAll();
	}

	public PageInfo<Area> queryArea(String code, String name, String parentCode, int pageNum, int pageSize){
		PageHelper.startPage(pageNum, pageSize);
		List<Area> areaList = areaMapper.queryArea(code, name, parentCode);
		PageInfo<Area> pageInfo = new PageInfo<>(areaList);
		return pageInfo;
	}

	/**
	 * 根据省份地市区县code查询对应名称
	 * @param septor 返回结果连接符号，可以为空
	 * @param province
	 * @param city
	 * @param county
	 * @return
	 */
	public String findNameByCode(String septor, String province, String city, String county){
		return areaMapper.findNameStrByCode(septor, province, city, county);
	}

	/**
	 * 根据区域code查询名称
	 * 首先去Redis查询，查询不到再去数据库查询
	 * @param code
	 * @return
	 */
	public String findNameByCode(String code) {
		if (null != code && !"".equals(code)){
			String name = null;//区域名称
			/*String key = "Area_" + code;
			Object object = redisUtil.get(key);
			if (object != null) {
				name = object.toString();
			} else {*/
				name = areaMapper.findNameByCode(code);
			/*	redisUtil.set(key, name);
			}*/
			return name;
		} else {
			return null;
		}

	}

	public Map<String,Object> findAreaByCode(String code){
		Area area3 = areaMapper.findAreaByCode(code);//区
		Area area2 = areaMapper.findAreaByCode(area3.getParentCode());//市
		Map<String,Object> map = new HashMap<>();
		map.put("province",area2.getParentCode());
		map.put("city",area2.getCode());
		map.put("county",code);
		return map;
	}
}
