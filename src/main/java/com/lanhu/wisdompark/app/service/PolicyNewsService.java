package com.lanhu.wisdompark.app.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.PolicyNews;
import com.lanhu.wisdompark.app.mapper.PolicyNewsMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/6/26
 */
@Service
public class PolicyNewsService {

    @Resource
    private PolicyNewsMapper policyNewsMapper;

    public int addPolicyNews(PolicyNews policyNews){
        return policyNewsMapper.add(policyNews);
    }

    public int editPolicyNews(PolicyNews policyNews){
        return policyNewsMapper.update(policyNews);
    }

    public PageInfo<PolicyNews> findPolicyNews(String gardenId,int pageNum,int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        Map<String,Object> map = new HashMap<>();
        map.put("gardenId",gardenId);
        List<PolicyNews> policyNews = policyNewsMapper.find(map);
        PageInfo<PolicyNews> pageInfo = new PageInfo<>(policyNews);
        return pageInfo;
    }

    public PolicyNews queryPolicyNews(int id){
        return policyNewsMapper.getPolicyNewsById(id);
    }
}
