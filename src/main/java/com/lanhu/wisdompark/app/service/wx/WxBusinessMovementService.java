package com.lanhu.wisdompark.app.service.wx;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.BusinessMovement;
import com.lanhu.wisdompark.app.domain.WpResource;
import com.lanhu.wisdompark.app.mapper.BusinessMovementMapper;
import com.lanhu.wisdompark.app.mapper.ResourceMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author whongyu
 * @create by 2019/5/22
 */
@Service
public class WxBusinessMovementService {

    @Resource
    private BusinessMovementMapper businessMovementMapper;
    @Resource
    private ResourceMapper resourceMapper;

    /**
     * 查询该企业下的所有动态
     * @param companyId
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageInfo<BusinessMovement> findAllMovementByCompanyId(String companyId, int pageNum, int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<BusinessMovement> businessMovements =businessMovementMapper.findAllMovementByCompanyId(companyId);
        PageInfo<BusinessMovement> pageInfo = new PageInfo<>(businessMovements);
        return pageInfo;
    }

    /**
     * 根据企业动态id查看动态详情
     * @param id
     * @return
     */
    public BusinessMovement findMovementById(String id){
        BusinessMovement businessMovement =businessMovementMapper.findMovementById(id);
        List<WpResource> resource = resourceMapper.getResource(businessMovement.getId(), 0);
        if(resource.size() > 0 && resource != null){
            businessMovement.setWpResourceList(resource);
        }
        return businessMovement;
    }
}
