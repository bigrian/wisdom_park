package com.lanhu.wisdompark.app.service.wx;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.domain.*;
import com.lanhu.wisdompark.app.mapper.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 微信小程序 - 园区相关service
 * @author whongyu
 * @create by 2019/5/21
 */
@Service
public class WxGardenInfoService {

    @Resource
    private GardenInfoMapper gardenInfoMapper;
    @Resource
    private WxAppUserMapper wxAppUserMapper;
    @Resource
    private VisitGardenMapper visitGardenMapper;
    @Resource
    private CompanyInfoMapper companyInfoMapper;
    @Resource
    private ResourceMapper resourceMapper;
    @Resource
    private PolicyNewsMapper policyNewsMapper;
    @Resource
    private UserGardenUnitMapper userGardenUnitMapper;
    @Resource
    private GardenUnitMapper gardenUnitMapper;

    /**
     * 查询所有 园区列表（分页）
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageInfo<GardenInfo> findAllUserDown(String gardenId, int pageNum,int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<GardenInfo> companyInfos = gardenInfoMapper.findAllUserDown(gardenId);
        PageInfo<GardenInfo> pageInfo = new PageInfo<>(companyInfos);
        return pageInfo;
    }

    /**
     * 园区详情页面
     * @param gardenId
     * @param userId
     * @return
     */
    public GardenInfo findGardenById(String gardenId,int userId){
        GardenInfo garden = gardenInfoMapper.findGardenById(gardenId);//园区相关信息

        VisitGarden visitGarden = new VisitGarden();
        visitGarden.setGardenId(gardenId);
        visitGarden.setWxAppUserId(userId);
        visitGardenMapper.addVisitGarden(visitGarden);

        List<WxAppUser> wxAppusers = wxAppUserMapper.getVisitInfo(gardenId);//根据园区id查出的最新八条访问者信息
        garden.setWxAppUserList(wxAppusers);

        List<CompanyInfo> companys = companyInfoMapper.findAllCompanyByGardenId(gardenId);//该园区入驻企业信息 有logo
        garden.setCompanyInfoList(companys);

        List<WpResource> resource = resourceMapper.getResource(gardenId, 4);//楼层落位图
        garden.setWpResourceList(resource);

        PolicyNews newest = policyNewsMapper.getNewest(gardenId);//活动详情  园区最新一条含有一张首页图
        garden.setPolicyNews(newest);

        return garden;
    }

    /**
     * 园区下-首页访问量等统计接口
     * @return
     */
    public Map<String, Integer> visitCounts(String gardenId,int userId){
        VisitGarden visitGarden = new VisitGarden();
        visitGarden.setWxAppUserId(userId);
        visitGarden.setGardenId(gardenId);
        visitGardenMapper.addVisitGarden(visitGarden);
        int countVisit = visitGardenMapper.countVisit(gardenId);//园区访问者数  动态
        Map<String, Object> map1 = new HashMap<>();
        map1.put("gardenId",gardenId);
        map1.put("type",1);
        int countAppoint = userGardenUnitMapper.countAppoint(map1);//预约人次  动态
        int countCompany = companyInfoMapper.countCompany(gardenId);//入驻人次  动态
        Map<String, Integer> map = new HashMap<>();
        map.put("countVisit",countVisit);
        map.put("countAppoint",countAppoint);
        map.put("countCompany",countCompany);
        return map;
    }

    /**
     * 园区下查询 所有户型接口
     * @param gardenId
     * @return
     */
    public List<GardenUnit> findAllGardenUnit(String gardenId){
        Map<String, Object> map = new HashMap<>();
        map.put("gardenId",gardenId);
        map.put("sort","sort");
        map.put("order","asc");
        List<GardenUnit> gardenUnits = gardenUnitMapper.findAll(map);
        map.clear();
        //该户型访问次数
       for(GardenUnit g : gardenUnits){
           map.put("gardenId",gardenId);
           map.put("gardenUnitId",g.getId());
           map.put("type",0);
           int countAppoint = userGardenUnitMapper.countAppoint(map);//访问人次
           g.setCounts(countAppoint);
       }
        return gardenUnits;
    }

    /**
     * 根据id查询园区户型
     * @param gardenId
     * @param gardenUnitId
     * @return
     */
    public GardenUnit findGardenUnit(String gardenId,int gardenUnitId,int wxAppUserId){
        Map<String, Object> map = new HashMap<>();
        map.put("gardenId",gardenId);
        map.put("id",gardenUnitId);
        List<GardenUnit> gardenUnitList = gardenUnitMapper.findAll(map);

        UserGardenUnit userGardenUnit = new UserGardenUnit();
        userGardenUnit.setWxAppUserId(wxAppUserId);
        userGardenUnit.setType(0);
        userGardenUnit.setGardenId(gardenId);
        userGardenUnit.setGardenUnitId(gardenUnitId);
        userGardenUnitMapper.add(userGardenUnit);
        map.put("type",0);
        int appoint = userGardenUnitMapper.countAppoint(map);//访问人次
        map.clear();

        List<UserGardenUnit> headPath = userGardenUnitMapper.findVisitUnit(gardenId);//八条最新不同访问者头像

        if (gardenUnitList !=null && gardenUnitList.size() > 0){
            GardenUnit gardenUnit = gardenUnitList.get(0);
            gardenUnit.setHeadPaths(headPath);//八条最新不同访问者头像
            gardenUnit.setCounts(appoint);//访问人次
            return gardenUnit;
        }
        return null;
    }
}
