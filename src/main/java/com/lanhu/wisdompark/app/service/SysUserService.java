package com.lanhu.wisdompark.app.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lanhu.wisdompark.app.constant.ErrorCode;
import com.lanhu.wisdompark.app.domain.User;
import com.lanhu.wisdompark.app.mapper.SysUserMapper;
import com.lanhu.wisdompark.app.util.MD5Util;
import com.lanhu.wisdompark.app.util.Result;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/5/23
 */
@Service
public class SysUserService {

    @Resource
    private SysUserMapper sysUserMapper;

    public Result login(String userName, String password){
        Map<String ,Object> map = new HashMap<>();
        map.put("userName",userName);
        List<User> users = sysUserMapper.findUser(map);
        if(users.size() > 0 && users != null){
            String password1 = MD5Util.fromStringToMD5(password);
            map.put("password",password1);
            List<User> users1 = sysUserMapper.findUser(map);
            if(users1.size() > 0 && users1 != null){
                User user = users1.get(0);
                if(user.getStatus()==1){
                    return new Result(ErrorCode.E_20010);//账号被禁用
                }
                return new Result(ErrorCode.SUCCESS,user);
            }else {
                return new Result(ErrorCode.E_20008);//密码不正确
            }
        }else {
            return new Result(ErrorCode.E_20007);//用户名不存在
        }
    }

    public PageInfo<User> findAll(int pageNum,int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        Map<String ,Object> map = new HashMap<>();
        List<User> users = sysUserMapper.findUser(map);
        PageInfo<User> pageInfo = new PageInfo<>(users);
        return pageInfo;
    }

    public int updateUser(User user){
        if (user != null) {
            user.setUpdateTime(new DateTime().toString("yyyy-MM-dd HH:mm:ss"));
            String oldPassword = sysUserMapper.findById(user.getId()).getPassword();
            if (oldPassword.equals(user.getPassword())){
                user.setPassword(oldPassword);
            }else {
                user.setPassword(MD5Util.fromStringToMD5(user.getPassword()));
            }
            return sysUserMapper.update(user);
        } else {
            return 0;
        }
    }

    /**
     * 修改密码 接口
     * @param id
     * @param oldPass
     * @param newPass
     * @return
     */
    public Result changePassword(int id, String oldPass, String newPass){
        if(null == oldPass || null == newPass || "".equals(oldPass) || "".equals(newPass)){
            return new Result(ErrorCode.E_20016);//密码不能为空!
        }
        String pass = MD5Util.fromStringToMD5(oldPass);
        User u = sysUserMapper.findById(id);
        if(!u.getPassword().equals(pass)){
            return new Result(ErrorCode.E_20008); //密码输入错误！
        }

        newPass = MD5Util.fromStringToMD5(newPass);
        User user = new User();
        user.setId(id);
        user.setPassword(newPass);
        int update = sysUserMapper.update(user);
        if (update > 0){
            return new Result( ErrorCode.SUCCESS);
        }else {
            return new Result( ErrorCode.E_20025);
        }
    }


    public int addUser(User user){
        Map<String,Object> map = new HashMap<>();
        map.put("userName",user.getUserName());
        List<User> user1 = sysUserMapper.findUser(map);
        if (user1 != null && user1.size()>0){
            return -1;
        }
        user.setPassword(MD5Util.fromStringToMD5(user.getPassword()));//md5加密
        return sysUserMapper.add(user);
    }

    public User queryUser(int id){
        return sysUserMapper.findById(id);
    }
}
