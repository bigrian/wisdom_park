package com.lanhu.wisdompark.app.service;

import com.lanhu.wisdompark.app.domain.Classify;
import com.lanhu.wisdompark.app.mapper.ClassifyMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author whongyu
 * @create by 2019/6/22
 */
@Service
public class ClassifyService {

    @Resource
    private ClassifyMapper classifyMapper;

    public int addClassify(Classify classify){
        return classifyMapper.add(classify);
    }

    public int editClassify(Classify classify){
        return classifyMapper.update(classify);
    }

    public int removeClassify(int id){
        return classifyMapper.remove(id);
    }

    public List<Classify> findClassify(int type){
        Map<String,Object> map = new HashMap<>();
        map.put("type",type);
        return classifyMapper.find(map);
    }

    public List<Classify> queryClassify(int parentId){
        Map<String,Object> map = new HashMap<>();
        map.put("parentId",parentId);
        return classifyMapper.find(map);
    }

    public Classify searchClassify(int id){
        return classifyMapper.get(id);
    }

    public List<Classify> findClassifyGarde(int type){
        Map<String, Object> map = new HashMap<>();
        map.put("parentId",1);
        map.put("type",type);
        List<Classify> classifies1 = classifyMapper.find(map);//一级分类
        System.out.println("我是map:"+map);
        map.clear();
        for(Classify c : classifies1){
            map.put("parentId",c.getId());
            map.put("type",type);
            List<Classify> classifies2 = classifyMapper.find(map);
            c.setClassifies(classifies2);
        }
        return classifies1;
    }
}
