package com.lanhu.wisdompark.app.util;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class TimeUtils {

    public static Boolean compareTime(String time1,Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date date1 = null;
        try {
            date1 = sdf.parse(time1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        int i = cal.get(Calendar.HOUR_OF_DAY) * 3600 + cal.get(Calendar.MINUTE) * 60 + cal.get(Calendar.SECOND); // 当前时间长度
        cal.setTime(date);
        int j = cal.get(Calendar.HOUR_OF_DAY) * 3600 + cal.get(Calendar.MINUTE) * 60 + cal.get(Calendar.SECOND); // 当前时间长度
        if(i >= j){
            return true;
        }else
            return false;
//        return  new Date(Long.valueOf(time1)).after(date);
    }

    public static String currentTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar cal = Calendar.getInstance();
        return sdf.format(cal.getTime());
    }

	public static String getDateTime(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return sdf.format(cal.getTime());
	}

	public static String validateTime(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE,5);
		return sdf.format(cal.getTime());
	}

	public static String currentHour(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHH");
		Calendar cal = Calendar.getInstance();
		return sdf.format(cal.getTime());
	}

    /**
	 * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String formatDate(Date date, Object... pattern) {
		String formatDate = null;
		if (pattern != null && pattern.length > 0) {
			formatDate = DateFormatUtils.format(date, pattern[0].toString());
		} else {
			formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
		}
		return formatDate;
	}
	
	public int getSecondLastedTime(Date startDate) {
		  long a = new Date().getTime();
		  long b = startDate.getTime();
		  int c = (int)((a - b) / 1000);
		  return c;
	}
	/**
	 * 根据时间得到天（yyyy-MM-dd HH:mm:ss）
	 */
    public static String getDay() {
	    Date currentTime = new Date();
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String dateString = formatter.format(currentTime);
	    String day;
	    day = dateString.substring(0, 10);
	    return day;
   }
    /**
	 * 得到天的时间（yyyy-MM-dd）
	 */
    public static Date getDateDay(String str) {
    	String dateStr = str.substring(0, 10);
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    Date date = new Date();
	    try {
			date = formatter.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    return date;
   }
	/**
	 * 得到现在小时
	 */
    public static String getHour() {
	    Date currentTime = new Date();
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String dateString = formatter.format(currentTime);
	    String hour;
	    hour = dateString.substring(11, 13);
	    return hour;
   }

	public static int compare_date(String DATE1, String DATE2) {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date dt1 = df.parse(DATE1);
			Date dt2 = df.parse(DATE2);
			if (dt1.getTime() > dt2.getTime()) {
				System.out.println("dt1 在dt2前");
				return 1;
			} else if (dt1.getTime() < dt2.getTime()) {
				System.out.println("dt1在dt2后");
				return -1;
			} else {
				return 0;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return 0;
	}
}
