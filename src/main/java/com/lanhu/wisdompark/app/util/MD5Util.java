package com.lanhu.wisdompark.app.util;

import java.security.MessageDigest;

public class MD5Util {

	public static String fromStringToMD5(String str) {
		try {
			char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'A', 'B', 'C', 'D', 'E', 'F' };
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(str.getBytes());
			byte[] md = mdInst.digest();
			int j = md.length;
			char chars[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				chars[k++] = hexDigits[byte0 >>> 4 & 0xf];
				chars[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(chars);
		}catch (Exception e){
			return null;
		}
	}
	
	public static void main(String args[]){
		String pass = "1";
		try {
			System.out.println(MD5Util.fromStringToMD5(pass));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
