package com.lanhu.wisdompark.app.util;



import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * @author Li Fuxiang
 * @Description: 字符串工具类
 * @Company:
 * @date 2016/10/9
 */
public class StringUtil {

	/**
	 * 判断字符串是否为空
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isEmpty(Object obj) {
		return obj == null || "".equals(obj.toString());
	}

	/**
	 * 替换字符中的空格
	 * 
	 * @param str
	 * @param replacement
	 * @return
	 */
	public static String replaceSpace(String str, String replacement) {
		return str.replaceAll("[\\p{Space}]+", replacement);
	}

	/**
	 * 替换字符中的标点符号
	 * 
	 * @param str
	 * @param replacement
	 * @return
	 */
	public static String replacePunct(String str, String replacement) {
		return str.replaceAll("[\\p{Punct}\\pP]+", replacement);
	}

	/**
	 * 清除符号及空格
	 * 
	 * @param str
	 * @return
	 */
	public static String clear(String str) {
		return replaceSpace(replacePunct(str, ""), "");
	}

	/**
	 * 截取指定长度的字符串
	 * 
	 * @param str
	 * @param end
	 * @return
	 */
	public static String subString(String str, int end) {

		if (StringUtils.isEmpty(str) || str.length() < end)
			return str;

		return str.substring(0, end);
	}

	/**
	 * 截掉最后的字符
	 * 
	 * @param sourceStr
	 * @param endStr
	 * @return
	 */
	public static String replaceEndStr(String sourceStr, String endStr) {
		if (sourceStr.endsWith(endStr)) {
			return sourceStr.substring(0, sourceStr.lastIndexOf(endStr));
		}
		return sourceStr;
	}

	/**
	 * trim全角空格
	 * 
	 * @param str
	 * @return
	 */
	public static String trim(String str) {

		if (StringUtils.isEmpty(str))
			return str;

		String regStartSpace = "^[　 ]*";
		String regEndSpace = "[　 ]*$";
		String strDelSpace = str.replaceAll(regStartSpace, "").replaceAll(regEndSpace, "");

		return strDelSpace.trim();
	}

	/**
	 * 首字母小写
	 * 
	 * @param str
	 * @return
	 */
	public static String firstToLowerCase(String str) {
		if (StringUtils.isEmpty(str))
			return str;
		return str.substring(0, 1).toLowerCase() + str.substring(1);
	}

	/**
	 * 首字母大写
	 * 
	 * @param str
	 * @return
	 */
	public static String firstToUpperCase(String str) {
		if (StringUtils.isEmpty(str))
			return str;
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	
	/**
	 * 清除HTML标签
	 * @param str
	 * @return
	 */
	public static String clearHtml(String str) {
		return str.replaceAll("<[^>]*>","");
	}
	
	/**
	 * 判断src使用逗号分割后是否包含str
	 * 
	 * @param src
	 * @param str
	 * @return
	 */
	public static boolean containWithComma(String src, String str) {
		return Arrays.asList(src.split(", ")).contains(str) || Arrays.asList(src.split(",")).contains(str);
	}
}
