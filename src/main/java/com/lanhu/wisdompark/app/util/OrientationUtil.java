package com.lanhu.wisdompark.app.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author whongyu
 * @create by 2019/5/21
 */
@Slf4j
public class OrientationUtil {

    private static String key="2KFBZ-3QXCJ-LFBF7-FJMDV-NQLCQ-OZFZ4";//腾讯地图 密钥

    public static String getAdress(double longitude,double latitude){
        log.info("腾讯-根据经纬度获取地址方法调用成功!"+longitude,latitude);
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://apis.map.qq.com/ws/geocoder/v1/?location="+latitude+","+longitude+"&key="+key+"&get_poi=1";
        log.info("腾讯-根据经纬度获取地址url:"+url);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        String body = responseEntity.getBody();
        JSONObject obj=JSON.parseObject(body);
        String address = obj.getJSONObject("result").getJSONObject("address").toString();
        log.info("腾讯-根据经纬度获取地址为:"+address);
        return address;
    }

    public static JSONObject getLongLat(String address){
        log.info("腾讯-根据地址获取经纬度调用成功!"+address);
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://apis.map.qq.com/ws/geocoder/v1/?address="+address+"&key="+key;//定位到门址
        log.info("腾讯-根据地址获取经纬度url:"+url);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        String body = responseEntity.getBody();
        JSONObject obj=JSON.parseObject(body);
        JSONObject object = obj.getJSONObject("result").getJSONObject("location");
        log.info("腾讯-根据地址获取经纬度为:"+object);
        return object;
    }
}
