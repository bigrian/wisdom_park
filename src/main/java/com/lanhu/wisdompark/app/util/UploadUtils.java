package com.lanhu.wisdompark.app.util;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.OperationContext;
import com.microsoft.azure.storage.blob.*;

import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class UploadUtils {

    public static String ROOT = "/home/data/fileupload/images"; //For Run
    public static String blobRoot = "https://elfstorage.blob.core.chinacloudapi.cn/parkfile"; //微软云blob存储根目录
    //存储文件到微软云BLOB安全字符串，里面包括账号和秘钥
    public static final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=elfstorage;AccountKey=PhAxc7gQV4+wEbaUmRgEf9fPQb5JWyhdr9BpwObpinhoHF4SRytlUmOq0ZGIx9PMnEcOb96a5aLg6uHjVx7SIA==;EndpointSuffix=core.chinacloudapi.cn";
 
    //上传的文件大小限制 (0-不做限制) ，单位：字节
    private long maxSize = 0;
    
    //允许上传的文件后缀，如：".jpg|.png|.git|.jpeg"，为空不做限制
    private String exts;
 
    //保存根路径，会在tomcat的webapps自动创建该文件夹
    private static String rootPath = "/home/data/fileupload/images/";
//    private static String rootPath = "D:/fileupload/images/";

    //保存路径，如 "userimage"
    private String savePath = "";
 
    //子目录创建方式，默认：年-月
    private  String subName = "yyyy-MM";
 
    //是否启动时间格式的子目录
    private boolean isSubName = false;
 
    //上传的文件名称
    private List<String> fileNames;
 
    //上传错误信息
    private String error;
 
    public UploadUtils() {
        this.fileNames = new ArrayList<String>();
    }

	public long getMaxSize() {
        return maxSize;
    }
 
    public void setMaxSize(long maxSize) {
        this.maxSize = maxSize;
    }
 
    public String getExts() {
        return exts;
    }
 
    public void setExts(String exts) {
        this.exts = exts;
    }
 
    private static String getRootPath() {
        return rootPath;
    }
 
    public String getSavePath() {
        return savePath;
    }
 
    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }
 
    public String getSubName() {
        return subName;
    }
 
    public void setSubName(String subName) {
        this.subName = subName;
    }
 
    public boolean getIsSubName() {
        return isSubName;
    }
 
    public void setIsSubName(boolean isSubName) {
        this.isSubName = isSubName;
    }
 
    public String getError() {
        return error;
    }
 
    public void setError(String error) {
        this.error = error;
    }
 
    public List<String> getFileNames() {
        return fileNames;
    }
 
    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }

    private String filePath(){
        String currentHour = TimeUtils.currentHour();
        String path = "";
        path = File.separator + this.getSavePath() + File.separator + currentHour;
//        ROOT = ROOT + File.separator +path;
        File directory = new File(ROOT+ File.separator +this.getSavePath());
        if (!directory.exists()){
            directory.mkdirs();
        }
        File directory1 = new File(ROOT+ path);
        if (!directory1.exists()){
            directory1.mkdirs();
        }
        return path;
    }
    
    /**
     * 检查文件大小是否合法
     * @param size 文件大小，单位/字节
     * @return boolean
     */
    private boolean checkSize(long size) {
        return !(size > this.getMaxSize()) || (0 == this.getMaxSize());
    }
 
    /**
     * 检查上传的文件后缀是否合法
     * @param ext 后缀
     * @return boolean
     */
    private boolean checkExt(String ext){
        if(this.getExts().isEmpty()){
            return true;
        }else{
            if(this.getExts().indexOf(ext) != -1){
                return true;
            }
        }
        return false;
    }
 
    /**
    * 返回Tomcat的webapps根目录
    * @return String 路径
    */
    private static String tomcatPath(){
        //获取当前项目的运行环境根目录,如：/E:/workspace/axkf/target/classes/
        String projectPath = "";
		try {
			projectPath = ResourceUtils.getURL("classpath:").getPath();
			if(projectPath.startsWith("file:")) {
				projectPath=projectPath.substring(5);
			}
			int index = projectPath.indexOf("manage");
			projectPath = projectPath.substring(0, index);
			//projectPath += "../../../";
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        //返回Tomcat的webapps根目录 (考虑到每次发布会覆盖war，文件最好存在war外面)
        return projectPath;
    }
    
    /**
     * 是否生成子目录，返回子目录名称
     * @return String 目录名称
     */
    private String dateDir(){
        //是否生成子目录
        String dateDir;
        if(this.getIsSubName()) {
            //设置文件存放子目录
            SimpleDateFormat df = new SimpleDateFormat(this.getSubName());// 设置日期格式
            dateDir = df.format(new Date());// new Date()为获取当前系统时间
            dateDir = dateDir + "/";
        }else{
            dateDir = "";
        }
        return dateDir;
    }
    /**
     * 返回上传文件根目录
     * @return String 路径
     */
 	public static String getTomcatPath() {
         return tomcatPath();
 	}
 	/**
     * 返回上传文件根目录
     * @return String 路径
     */
 	public static String getUploadPath() {
 		//获取Tomcat的webapps根目录 
         String projectPath = tomcatPath();
 		//web服务器根目录文件路径
         String webFilePath = getRootPath();
         return projectPath+webFilePath;
 	}

    public String uploadBlob(MultipartFile file, String containerName) {
        CloudStorageAccount storageAccount;
        CloudBlobClient blobClient = null;
        CloudBlobContainer container=null;
        String fileName = "";
        try{
            File sourceFile = null;
            // Parse the connection string and create a blob client to interact with Blob storage
            storageAccount = CloudStorageAccount.parse(storageConnectionString);
            blobClient = storageAccount.createCloudBlobClient();
            container = blobClient.getContainerReference(containerName);

            // Create the container if it does not exist with public access.
            System.out.println("Creating container: " + container.getName());
            container.createIfNotExists(BlobContainerPublicAccessType.CONTAINER, new BlobRequestOptions(), new OperationContext());


            fileName = file.getOriginalFilename();
            String suffixName = fileName.substring(fileName.lastIndexOf("."));

            fileName = TimeUtils.formatDate(new Date(), "yyyyMMddHHmmss")+(int)((Math.random()*9+1)*100000)+ suffixName;

            Files.copy(file.getInputStream(), Paths.get(ROOT+filePath(), fileName));
            //Getting a blob reference
            CloudBlockBlob blob = container.getBlockBlobReference(fileName);
            blob.uploadFromFile(ROOT+filePath()+File.separator+fileName);
        }catch(Exception e){
            e.printStackTrace();
        }
        return blobRoot + File.separator + fileName;
    }
}
